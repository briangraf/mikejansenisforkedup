﻿namespace CommonFramework.Configuration
{
    using System;
    using System.Collections.Generic;
    using System.Linq;

    /// <summary>
    /// Name-value pairs for configuration environment; holds both values sent in by client (user, 
    /// account, workstation, etc.) and values sent in by the system (application, etc)
    /// </summary>
    [Serializable]
    public class EnvironmentMap : Dictionary<string, string>
    {
        #region Private Fields

        private readonly IConfigConfig _configConfig;

        #endregion

        #region Constructors

        public EnvironmentMap(IConfigConfig configConfig, IDictionary<string, string> map)
            : base(map)
        {
            _configConfig = configConfig;
        }

        public EnvironmentMap(IDictionary<string, string> map)
            : this(new ConfigConfig(), map)
        {
        }

        public EnvironmentMap(IConfigConfig configConfig)
        {
            _configConfig = configConfig;
        }

        public EnvironmentMap() : this(new ConfigConfig()) { }

        #endregion

        #region Public Properties

        public string Server
        {
            get { return this[_configConfig.WellKnownLevelServer]; }
            set { this[_configConfig.WellKnownLevelServer] = value; }
        }

        public string Application
        {
            get { return this[_configConfig.WellKnownLevelApplication]; }
            set { this[_configConfig.WellKnownLevelApplication] = value; }
        }

        public string Account
        {
            get { return this[_configConfig.WellKnownLevelAccount]; }
            set { this[_configConfig.WellKnownLevelAccount] = value; }
        }

        public string Group
        {
            get { return this[_configConfig.WellKnownLevelGroup]; }
            set { this[_configConfig.WellKnownLevelGroup] = value; }
        }

        public string User
        {
            get { return this[_configConfig.WellKnownLevelUser]; }
            set { this[_configConfig.WellKnownLevelUser] = value; }
        }

        public string Workstation
        {
            get { return this[_configConfig.WellKnownLevelWorkstation]; }
            set { this[_configConfig.WellKnownLevelWorkstation] = value; }
        }

        #endregion

        #region Public Methods

        /// <summary>
        /// Remove any values sent in by the client that should only be set server-side
        /// </summary>
        public void StripSystemValues()
        {
            List<string> systemKeys = new List<string>(
                Keys.Where(k => k.StartsWith(_configConfig.SystemLevelPrefix, StringComparison.InvariantCulture)));
            foreach (string key in systemKeys)
            {
                Remove(key);
            }
        }

        #endregion
    }

}

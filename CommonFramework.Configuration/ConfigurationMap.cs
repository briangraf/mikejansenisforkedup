﻿namespace CommonFramework.Configuration
{
    using System.Collections;
    using System.Collections.Generic;
    using CommonFramework;

    public class ConfigurationMap : IConfigurationMap, IConfigurationSource
    {
        private readonly Dictionary<string, string> _configurationMap = new Dictionary<string, string>();

        public string this[string key]
        {
            get
            {
                return _configurationMap[key];
            }
        }

        public T Get<T>(string key)
        {
            return Get<T>(key, default(T));
        }

        public T Get<T>(string key, T defaultValue)
        {
            T value;
            string rawValue;

            if (!_configurationMap.TryGetValue(key, out rawValue))
            {
                return defaultValue;
            }
            return rawValue.TryGetValue<T>(out value) ? value : defaultValue;
        }

        public void Add(IConfigurationSource source)
        {
            foreach (KeyValuePair<string,string> entry in source)
            {
                _configurationMap[entry.Key] = entry.Value;
            }
        }

        public IEnumerator<KeyValuePair<string, string>> GetEnumerator()
        {
            return _configurationMap.GetEnumerator();
        }

        IEnumerator IEnumerable.GetEnumerator()
        {
            return _configurationMap.GetEnumerator();
        }

    }
}

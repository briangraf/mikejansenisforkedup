﻿namespace CommonFramework.Configuration
{
    using System.Configuration;
    using CommonFramework.Container;

    public class AppSettingsSource : ConfigurationSourceBase
    {
        public AppSettingsSource()
        {
            foreach (string settingKey in ConfigurationManager.AppSettings.AllKeys)
            {
                Settings[settingKey] = ConfigurationManager.AppSettings[settingKey];
            }
        }

        public static void RegisterConfiguration(IContainerManager cm)
        {
            ConfigurationMap configurationMap = cm.GetInstance<ConfigurationMap>();
            configurationMap.Add(cm.GetInstance<AppSettingsSource>());
            cm.Bind<IConfigurationMap>(configurationMap);
        }

    }
}

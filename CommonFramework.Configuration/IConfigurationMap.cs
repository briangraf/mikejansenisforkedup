﻿namespace CommonFramework.Configuration
{
    using System.Collections.Generic;
    
    public interface IConfigurationMap : IEnumerable<KeyValuePair<string, string>>
    {
        string this[string key] { get; }
        T Get<T>(string key);
        T Get<T>(string key, T defaultValue);
    }
}

﻿namespace CommonFramework.Configuration
{
    public interface IConfigConfig
    {
        string PathSeparator { get; }

        string SystemLevelPrefix { get; }
        string AuthorizationNode { get; }

        string WellKnownLevelServer { get; }
        string WellKnownLevelApplication { get; }
        string WellKnownLevelAccount { get; }
        string WellKnownLevelGroup { get; }
        string WellKnownLevelUser { get; }
        string WellKnownLevelWorkstation { get; }
    }
}

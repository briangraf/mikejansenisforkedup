﻿namespace CommonFramework.Configuration
{
    /// <summary>
    /// Config related parameters
    /// </summary>
    public class ConfigConfig : IConfigConfig
    {
        #region IConfigConfig Implementation

        public string PathSeparator { get { return "."; } }
        public string SystemLevelPrefix { get { return "$"; } }

        public string AuthorizationNode { get { return "$Authorized"; } }

        public string WellKnownLevelServer { get { return "$Server"; } }
        public string WellKnownLevelApplication { get { return "Application"; } }
        public string WellKnownLevelAccount { get { return "$Account"; } }
        public string WellKnownLevelGroup { get { return "Group"; } }
        public string WellKnownLevelUser { get { return "$User"; } }
        public string WellKnownLevelWorkstation { get { return "Workstation"; } }

        #endregion

    }
}

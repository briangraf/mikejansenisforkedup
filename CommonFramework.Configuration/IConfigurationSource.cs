﻿namespace CommonFramework.Configuration
{
    using System.Collections.Generic;

    public interface IConfigurationSource : IEnumerable<KeyValuePair<string, string>>
    {
    }
}

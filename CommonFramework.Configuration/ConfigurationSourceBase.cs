﻿namespace CommonFramework.Configuration
{
    using System.Collections;
    using System.Collections.Generic;

    public abstract class ConfigurationSourceBase : IConfigurationSource
    {
        protected readonly Dictionary<string, string> Settings = new Dictionary<string, string>();

        public IEnumerator<KeyValuePair<string, string>> GetEnumerator()
        {
            return Settings.GetEnumerator();
        }

        IEnumerator IEnumerable.GetEnumerator()
        {
            return Settings.GetEnumerator();
        }
    }
}

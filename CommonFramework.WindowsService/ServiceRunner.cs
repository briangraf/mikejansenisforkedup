﻿namespace CommonFramework.WindowsService
{
    using System;
    using System.Reflection;
    using System.ServiceProcess;
    using CLAP;
    using NLog;
    using CommonFramework.Log;

    public abstract class ServiceRunner : ServiceBase
    {
        private static readonly Logger logger = LogManager.GetCurrentClassLogger();

        private class ClapRunner
        {
            private static readonly Logger logger = LogManager.GetCurrentClassLogger();

            private readonly ServiceRunner _serviceRunner;

            public ClapRunner(ServiceRunner serviceRunner)
            {
                _serviceRunner = serviceRunner;
            }

            [Verb(IsDefault=true, Description="Run as a Windows Service")]
            public void Service
                (
                    [Aliases("d"), Description("Break point for attaching to debugger")] bool debug
                )
            {
                MethodBase method = MethodBase.GetCurrentMethod();
                logger.Enter(method);

                try
                {
                    if (debug)
                    {
                        System.Diagnostics.Debugger.Break();
                    }
                    ServiceBase.Run(_serviceRunner);

                }
                catch (Exception ex)
                {
                    logger.ErrorException("Unhandled exception", ex);
                    throw;
                }
                finally
                {
                    logger.Exit(method);
                }
            }

            [Verb(Description="Run as a Console Application")]
            public void Console
                (
                    [Aliases("d"), Description("Break point for attaching to debugger")] bool debug
                )
            {
                MethodBase method = MethodBase.GetCurrentMethod();
                logger.Enter(method);

                try
                {
                    System.Console.WriteLine("Running service as a console application.");
                    if (debug)
                    {
                        System.Diagnostics.Debugger.Break();
                    }
                    _serviceRunner.OnStart(Environment.GetCommandLineArgs());
                    System.Console.WriteLine("Hit ENTER to stop.");
                    System.Console.ReadLine();
                    _serviceRunner.OnStop();

                }
                catch (Exception ex)
                {
                    logger.ErrorException("Unhandled exception", ex);
                    throw;
                }
                finally
                {
                    logger.Exit(method);
                }
            }
        }

        public int Run(string[] args)
        {
            MethodBase method = MethodBase.GetCurrentMethod();
            logger.Enter(method);

            try
            {
                return Parser.Run<ClapRunner>(args, new ClapRunner(this));

            }
            catch (Exception ex)
            {
                logger.ErrorException("Unhandled exception", ex);
                throw;
            }
            finally
            {
                logger.Exit(method);
            }
        }

        protected override abstract void OnStart(string[] args);

        protected override abstract void OnStop();

    }
}

﻿namespace CommonFramework
{
    using System;
    using System.Collections.Generic;

    /// <summary>
    /// Maps a tree structure into a dictionary with the fully qualified path as the key
    /// and each node as the value; used to speed up tree lookups
    /// </summary>
    /// <typeparam name="T"></typeparam>
    public class TreeMapper<T, TVal>
    {
        private readonly string _pathSeparator;
        private readonly Func<T, string> _pathFunction;
        private readonly Func<T, TVal> _valueFunction;
        private readonly Func<T, IEnumerable<T>> _enumeratorFunction;
        private readonly Predicate<T> _doMapPredicate;

        public TreeMapper(string pathSeparator, Func<T, string> pathFunction,
            Func<T, IEnumerable<T>> enumeratorFunction, Func<T, TVal> valueFunction,
            Predicate<T> doMapPredicate)
        {
            _pathSeparator = pathSeparator;
            _pathFunction = pathFunction;
            _enumeratorFunction = enumeratorFunction;
            _valueFunction = valueFunction;
            _doMapPredicate = doMapPredicate;
        }

        public Dictionary<string, TVal> Map(IEnumerable<T> rootList)
        {
            Dictionary<string, TVal> map = new Dictionary<string, TVal>();
            Map(map, rootList, null);
            return map;
        }

        public Dictionary<string, TVal> Map(T root)
        {
            Dictionary<string, TVal> map = new Dictionary<string, TVal>();
            Map(map, root, null);
            return map;
        }

        private void Map(Dictionary<string, TVal> map, IEnumerable<T> list, string parentPath)
        {
            foreach (T node in list)
            {
                Map(map, node, parentPath);
            }
        }

        private void Map(Dictionary<string, TVal> map, T node, string parentPath)
        {
            string nodePath = _pathFunction(node);
            string path = parentPath == null ? nodePath : string.Format("{0}{1}{2}",
                parentPath, _pathSeparator, nodePath);
            if (_doMapPredicate(node))
            {
                map[path] = _valueFunction(node);
            }
            IEnumerable<T> childList = _enumeratorFunction(node);
            if (childList != null)
            {
                Map(map, childList, path);
            }
        }

    }
}

﻿namespace CommonFramework
{
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using System.Reflection;
    using NLog;

    public static class ReflectionUtils
    {
        #region Private Fields

        private static readonly Logger logger = LogManager.GetCurrentClassLogger();
        private static readonly IList<string> IndentLevels = StringUtils.CreateIndentLevels("  ", 100);

        private static readonly object ReferencedAssembliesSyncRoot = new object();
        private static IEnumerable<Assembly> _ReferencedAssemblies;
        private static IEnumerable<Type> _ReferencedTypes ;

        #endregion

        #region Public Properties

        public static IEnumerable<Assembly> ReferencedAssemblies { get { return GetReferencedAssemblies(); } }
        public static IEnumerable<Type> ReferencedTypes { get { return GetReferencedTypes(); } }

        #endregion

        #region Public Methods

        public static IEnumerable<Type> GetImplementationTypes<T>()
        {
            Type interfaceType = typeof(T);
            return (from t in ReferencedTypes
                    where interfaceType.IsAssignableFrom(t) && !t.IsAbstract && !t.IsInterface
                    select t).ToList();
        }

        public static IEnumerable<Type> GetImplementationTypes<T>(Func<Type, bool> predicate)
        {
            return GetImplementationTypes<T>().Where(predicate);
        }

        public static Type GetImplementationType<T>()
        {
            return GetImplementationTypes<T>().FirstOrDefault();
        }

        public static Type GetImplementationType<T>(Func<Type, bool> predicate)
        {
            return GetImplementationTypes<T>(predicate).FirstOrDefault();
        }

        public static IEnumerable<T> GetImplementationInstances<T>()
        {
            IEnumerable<Type> types = GetImplementationTypes<T>();
            return CreateInstances<T>(types);
        }

        public static IEnumerable<T> GetImplementationInstances<T>(Func<Type, bool> predicate)
        {
            IEnumerable<Type> types = GetImplementationTypes<T>(predicate);
            return CreateInstances<T>(types);
        }

        public static T GetImplementationInstance<T>()
        {
            return (T)Activator.CreateInstance(GetImplementationType<T>());
        }

        public static T GetImplementationInstance<T>(Func<Type, bool> predicate)
        {
            return (T)Activator.CreateInstance(GetImplementationType<T>(predicate));
        }

        #endregion

        #region Private Methods

        private static IEnumerable<Assembly> GetReferencedAssemblies()
        {
            if (_ReferencedAssemblies == null)
            {
                lock (ReferencedAssembliesSyncRoot)
                {
                    if (_ReferencedAssemblies == null)
                    {
                        Dictionary<string, Assembly> list = new Dictionary<string, Assembly>();
                        IEnumerable<AssemblyName> assemblyNames =
                            AppDomain.CurrentDomain.GetAssemblies().Where(AssemblyPredicate).Select(a => a.GetName(true));
                        RecursiveBuildReferencedAssembliesList(list, assemblyNames, 0);
                        _ReferencedAssemblies = list.Values.Where(a => a != null).ToList();
                    }
                }
            }
            return _ReferencedAssemblies;
        }

        private static IEnumerable<Type> GetReferencedTypes()
        {
            if (_ReferencedTypes == null)
            {
                lock (ReferencedAssembliesSyncRoot)
                {
                    if (_ReferencedTypes == null)
                    {
                        _ReferencedTypes = 
                            ReferencedAssemblies.SelectMany(a => 
                            {
                                try
                                {
                                    return a.GetTypes();
                                }
                                catch (ReflectionTypeLoadException ex) 
                                {
                                    return new Type[] {};
                                }
                            }).ToList();
                    }
                }
            }
            return _ReferencedTypes;
        }

        private static void RecursiveBuildReferencedAssembliesList(IDictionary<string, Assembly> list, IEnumerable<AssemblyName> toSearch, int indent)
        {
            foreach (AssemblyName assemblyName in toSearch)
            {
                logger.Debug("Referenced Assembly: {0}{1}", IndentLevels[indent], assemblyName.FullName);
                if (!list.ContainsKey(assemblyName.FullName))
                {
                    try
                    {
                        Assembly assembly = Assembly.Load(assemblyName);
                        list[assemblyName.FullName] = assembly;
                        IEnumerable<AssemblyName> nextToSearch =
                            assembly.GetReferencedAssemblies().Where(AssemblyNamePredicate);
                        RecursiveBuildReferencedAssembliesList(list, nextToSearch, indent + 1);
                    }
                    catch (Exception ex)
                    {
                        list[assemblyName.FullName] = null;
                        logger.Debug("Referenced Assembly Exception: {0}{1}", IndentLevels[indent], ex.Message);
                    }
                }
            }
        }

        private static IEnumerable<T> CreateInstances<T>(IEnumerable<Type> types)
        {
            List<T> instances = new List<T>();
            foreach (Type type in types)
            {
                T instance = (T)Activator.CreateInstance(type);
                instances.Add(instance);
            }
            return instances;
        }

        private static bool AssemblyPredicate(Assembly assembly)
        {
            return AssemblyNamePredicate(assembly.GetName());
        }

        private static bool AssemblyNamePredicate(AssemblyName assemblyName)
        {
            return true /*assemblyName.Name != "mscorlib" &&
                    !assemblyName.Name.StartsWith("System") */;
        }

        #endregion

    }
}

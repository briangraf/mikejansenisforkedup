﻿namespace CommonFramework
{
    using System;
    using System.Collections.Generic;

    /// <summary>
    /// Used to simulate enums with other properties and values.
    /// </summary>
    /// <typeparam name="T"></typeparam>
    public abstract class Enumish<T> : IComparable<Enumish<T>>
    {
        private readonly T _value;
        
        public T Value { get { return _value; } }
        
        protected Enumish(T value) { _value = value; }
        
        public override string ToString()
        {
            return Value.ToString();
        }

        public virtual int CompareTo(Enumish<T> other)
        {
            return Comparer<Enumish<T>>.Default.Compare(this, other);
        }

    }

    public abstract class SimpleSortableEnumish<TValue, TSort>: Enumish<TValue>
    {
        private readonly TSort _sortKey;

        public TSort SortKey { get { return _sortKey; } }

        protected SimpleSortableEnumish(TValue value, TSort sortKey): base(value)
        {
            _sortKey = sortKey;
        }

        public override int CompareTo(Enumish<TValue> other)
        {
            return Comparer<TSort>.Default.Compare(this.SortKey, ((SimpleSortableEnumish<TValue,TSort>)other).SortKey);
        }


    }

}

﻿namespace CommonFramework
{
    using System;
    using System.Collections.Generic;
    using System.IO;
    using System.Linq;
    using System.Reflection;
    using System.Security.Cryptography;
    using System.Text;

    public abstract class TokenBase
    {
        public class EncryptionInfo
        {
            private readonly ICryptoTransform _encryptor;
            private readonly ICryptoTransform _decryptor;
            private readonly Random _random =
                new Random((int)(DateTime.UtcNow.Ticks % (long)int.MaxValue));

            public ICryptoTransform Encryptor { get { return _encryptor; } }
            public ICryptoTransform Decryptor { get { return _decryptor; } }
            public Random Random { get { return _random; } }

            public EncryptionInfo(string salt, string key) :
                this(salt, key, new RC2CryptoServiceProvider()) { }

            public EncryptionInfo(string salt, string key, SymmetricAlgorithm algorithm)
            {
                byte[] saltBytes = Encoding.ASCII.GetBytes(salt);
                Rfc2898DeriveBytes keyBytes = new Rfc2898DeriveBytes(key, saltBytes);
                algorithm.Key = keyBytes.GetBytes(algorithm.KeySize / 8);
                algorithm.IV = keyBytes.GetBytes(algorithm.BlockSize / 8);
                _encryptor = algorithm.CreateEncryptor(algorithm.Key, algorithm.IV);
                _decryptor = algorithm.CreateDecryptor(algorithm.Key, algorithm.IV);
            }
        }

        #region Abstract Members

        protected abstract bool IsKeyed { get; }
        protected abstract EncryptionInfo Encryption { get; }

        #endregion

        #region Public Properties

        public string Token { get; private set; }

        #endregion

        #region Protected Properties

        protected class KeyAttribute : Attribute
        {
            public KeyAttribute(string key) { Key = key; }
            public string Key { get; set; }
        }

        protected class IndexAttribute : Attribute
        {
            public IndexAttribute(int index) { Index = index; }
            public int Index { get; set; }
        }

        #endregion

        #region Private Static

        private static readonly string Base64TranslateFrom = "=/+";
        private static readonly string Base64TranslateTo = "@-_";

        private static readonly char PartDelimiter = '|';
        private static readonly char MapDelimiter = '=';
        private static readonly BindingFlags PropertyBindingFlags =
            BindingFlags.Instance | BindingFlags.Public;

        #endregion

        #region Encryption

        private string Encrypt(params string[] parts)
        {
            string toEncrypt = GetRandomString() + PartDelimiter.ToString() +
                string.Join(PartDelimiter.ToString(), parts);

            using (MemoryStream memoryStream = new MemoryStream())
            {
                using (CryptoStream encryptStream = new CryptoStream(memoryStream, Encryption.Encryptor, CryptoStreamMode.Write))
                using (StreamWriter writer = new StreamWriter(encryptStream))
                {
                    writer.Write(toEncrypt);
                }
                return Convert.ToBase64String(memoryStream.ToArray()).Translate(Base64TranslateFrom, Base64TranslateTo);
            }
        }

        private IList<string> Decrypt(string toDecrypt)
        {
            byte[] bytes = Convert.FromBase64String(toDecrypt.Translate(Base64TranslateTo, Base64TranslateFrom));
            using (MemoryStream memoryStream = new MemoryStream(bytes))
            using (CryptoStream decryptStream = new CryptoStream(memoryStream, Encryption.Decryptor, CryptoStreamMode.Read))
            using (StreamReader reader = new StreamReader(decryptStream))
            {
                string decrypted = reader.ReadToEnd();
                IList<string> partList = new List<string>(decrypted.Split(PartDelimiter));
                partList.RemoveAt(0);
                return partList;
            }
        }

        private string GetRandomString()
        {
            // Put small random string and then the date/time first since they
            // will differ at each login and since the symmetric algorithms use 
            // Cipher Block Chaining, making the first part different during 
            // each login will make the whole thing different.

            byte[] bytes = new byte[Encryption.Random.Next(3, 12)];
            Encryption.Random.NextBytes(bytes);
            return Convert.ToBase64String(bytes);
        }

        #endregion

        #region Constructors

        protected TokenBase()
        {
        }

        protected TokenBase(string token)
        {
            Initialize(token);
        }

        #endregion

        #region Private Methods

        private Dictionary<TKey, PropertyInfo> GetProperties<TKey, TAttr>(Func<TAttr, TKey> keyFunc)
        {
            PropertyInfo[] allProperties = GetType().GetProperties(PropertyBindingFlags);
            Dictionary<TKey, PropertyInfo> properties = new Dictionary<TKey, PropertyInfo>();
            foreach (PropertyInfo propertyInfo in allProperties)
            {
                object[] attributes = propertyInfo.GetCustomAttributes(typeof(TAttr), false);
                if (attributes.Length == 1)
                {
                    TKey key = keyFunc((TAttr)attributes[0]);
                    properties[key] = propertyInfo;
                }
            }

            return properties;
        }

        private Dictionary<int, PropertyInfo> GetIndexedProperties()
        {
            return GetProperties<int, IndexAttribute>(i => i.Index);
        }

        private Dictionary<string, PropertyInfo> GetKeyedProperties()
        {
            return GetProperties<string, KeyAttribute>(k => k.Key);
        }

        private void ProcessIndexedProperties(IList<string> partList)
        {
            Dictionary<int, PropertyInfo> indexedProperties = GetIndexedProperties();

            for (int index = 0; index < partList.Count; index++)
            {
                string part = partList[index];
                PropertyInfo indexedProperty;
                if (indexedProperties.TryGetValue(index, out indexedProperty))
                {
                    object objValue = indexedProperty.PropertyType != typeof(string)
                        ? Convert.ChangeType(part, indexedProperty.PropertyType)
                        : part;
                    indexedProperty.SetValue(this, objValue, null);
                }

            }
        }

        private void ProcessKeyedProperties(IList<string> partList)
        {
            Dictionary<string, PropertyInfo> keyedProperties = GetKeyedProperties();

            foreach (string part in partList)
            {
                int delimiterPos = part.IndexOf(MapDelimiter);
                if (delimiterPos > 0)
                {
                    string key = part.Substring(0, delimiterPos);
                    string value = part.Substring(delimiterPos + 1);
                    PropertyInfo keyedProperty;
                    if (keyedProperties.TryGetValue(key, out keyedProperty))
                    {
                        Type convertType =
                            (keyedProperty.PropertyType.IsGenericType &&
                            keyedProperty.PropertyType.GetGenericTypeDefinition() == typeof(Nullable<>))
                            ? keyedProperty.PropertyType.GetGenericArguments()[0]
                            : keyedProperty.PropertyType;

                        object objValue =
                            convertType == typeof(string)
                            ? value
                            : convertType == typeof(Guid)
                                ? Guid.Parse(value)
                                : convertType.IsEnum
                                    ? Enum.Parse(convertType, value)
                                    : Convert.ChangeType(value, convertType);

                        keyedProperty.SetValue(this, objValue, null);
                    }
                }

            }
        }

        private void InitializeKeyed()
        {
            List<string> parts = new List<string>();
            Dictionary<string, PropertyInfo> keyedProperties = GetKeyedProperties();
            foreach (KeyValuePair<string, PropertyInfo> pair in keyedProperties)
            {
                object value = pair.Value.GetValue(this, null);
                if (value != null)
                {
                    string part = string.Format("{0}{1}{2}",
                        pair.Key,
                        MapDelimiter,
                        value);
                    parts.Add(part);
                }
            }
            Token = Encrypt(parts.ToArray());
        }

        private void InitializeIndexed()
        {
            Dictionary<int, PropertyInfo> indexedProperties = GetIndexedProperties();

            List<int> sortedIndexes = new List<int>(indexedProperties.Keys);
            sortedIndexes.Sort();

            List<string> parts =
                new List<string>(new string[sortedIndexes[sortedIndexes.Count - 1] + 1]);
            foreach (int index in sortedIndexes)
            {
                object value = indexedProperties[index].GetValue(this, null);
                if (value != null)
                {
                    parts[index] = value.ToString();
                }
            }
            Token = Encrypt(parts.ToArray());

        }

        #endregion

        #region Public Methods

        public void Initialize()
        {
            if (IsKeyed)
            {
                InitializeKeyed();
            }
            else
            {
                InitializeIndexed();
            }
        }

        public void Initialize(string token)
        {
            Token = token;
            IList<string> partList;
            if (token[0] == '*')
            {
                partList = token.Substring(1).Split(PartDelimiter);
                Token = Encrypt(partList.ToArray());
            }
            else
            {
                partList = Decrypt(token);
            }

            if (IsKeyed)
            {
                ProcessKeyedProperties(partList);
            }
            else
            {
                ProcessIndexedProperties(partList);
            }
        }

        public override string ToString()
        {
            StringBuilder builder = new StringBuilder();
            builder.Append(this.GetType().Name);
            if (IsKeyed)
            {
                Dictionary<string, PropertyInfo> properties = GetKeyedProperties();
                foreach(KeyValuePair<string, PropertyInfo> property in properties)
                {
                    object value = property.Value.GetValue(this);
                    if (value != null)
                    {
                        builder.AppendFormat("|{0}[1]={2}", property.Value.Name, property.Key, value);
                    }
                }
            }
            else
            {
                Dictionary<int, PropertyInfo> properties = GetIndexedProperties();
                foreach (KeyValuePair<int,PropertyInfo> property in properties)
                {
                    object value = property.Value.GetValue(this);
                    if (value != null)
                    {
                        builder.AppendFormat("|[{0}]={1}", property.Key, value);
                    }
                }
            }

            return builder.ToString();
        }

        #endregion

    }
}

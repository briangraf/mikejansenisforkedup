﻿namespace CommonFramework.Log
{
    using System;
    using System.IO;
    using System.Reflection;
    using System.Text;
    using System.Threading;
    using CommonFramework.Stream;
    using NLog;

    public static class NLogExtensions
    {
        private static string FormatMethod(MethodBase method)
        {
            return string.Format("{0}.{1}", method.DeclaringType.FullName, method.Name);
        }

        private static void LogParameters(Logger logger, params object[] parameters)
        {
            for (int index = 0; index < parameters.Length - 1; index += 2)
            {
                logger.Debug("Parameter: {0} = {1}", parameters[index], parameters[index + 1] ?? "null");
            }
        }

        public static void Enter(this Logger logger, MethodBase method, params object[] parameters)
        {
            logger.Trace("ENTER {0}", FormatMethod(method));
            if (parameters != null && parameters.Length > 0)
            {
                LogParameters(logger, parameters);
            }
        }

        public static void Exit(this Logger logger, MethodBase method, params object[] parameters)
        {
            logger.Trace("EXIT {0}", FormatMethod(method));
            if (parameters != null && parameters.Length > 0)
            {
                LogParameters(logger, parameters);
            }
        }

        public static void WriteBuffer(this Logger logger, LogLevel logLevel, byte[] buffer, int offset, int count, string format, params object[] arguments)
        {
            if (logger.IsEnabled(logLevel))
            {
                StringBuilder builder = new StringBuilder();

                if (!string.IsNullOrWhiteSpace(format))
                {
                    builder.AppendFormat(format, arguments);
                    builder.AppendLine();
                }

                StringBuilder hexBuilder = new StringBuilder();
                StringBuilder charBuilder = new StringBuilder();

                for (int outerIndex = offset;
                        outerIndex < offset + count && outerIndex < buffer.Length;
                        outerIndex++)
                {
                    hexBuilder.AppendFormat(" {0:X2}", buffer[outerIndex]);
                    char[] chars = Encoding.UTF8.GetChars(buffer, outerIndex, 1);
                    char oneChar = char.IsControl(chars[0]) ? '.' : chars[0];
                    charBuilder.AppendFormat("{0}", oneChar);

                    if (outerIndex % 16 == 0)
                    {
                        builder.AppendFormat("{1,48} | {2,16}{0}",
                            Environment.NewLine,
                            hexBuilder.ToString(),
                            charBuilder.ToString());
                        hexBuilder = new StringBuilder();
                        charBuilder = new StringBuilder();
                    }
                }

                if (hexBuilder.Length > 0)
                {
                    builder.AppendFormat("{1,-48}{2,-16}{0}",
                        Environment.NewLine,
                        hexBuilder.ToString(),
                        charBuilder.ToString());
                }

                logger.Log(logLevel, builder.ToString());
            }
        }

        public static void WriteStream(this Logger logger, LogLevel logLevel, Stream stream, bool isRewind, 
            string format, params object[] arguments)
        {
            if (!string.IsNullOrWhiteSpace(format))
            {
                logger.Log(logLevel, format, arguments);
            }

            byte[] buffer = new byte[1024 * 10];
            stream.ForEachBlock(buffer, isRewind, (buf, len, isEof) =>
            {
                logger.WriteBuffer(logLevel, buffer, 0, len, null);
                return true;
            });
        }

        public static void WriteStreamAsText(this Logger logger, LogLevel logLevel, Stream stream, bool isRewind, 
            string format, params object[] arguments)
        {
            if (!string.IsNullOrWhiteSpace(format))
            {
                logger.Log(logLevel, format, arguments);
            }

            MemoryStream memoryStream = new MemoryStream();
            byte[] buffer = new byte[1024 * 10];
            stream.ForEachBlock(buffer, isRewind, (buf, len, isEof) =>
            {
                memoryStream.Write(buf, 0, len);
                return true;
            });
            using (StreamReader reader = new StreamReader(memoryStream))
            {
                logger.Log(logLevel, reader.ReadToEnd());
            }
        }

        public static void FullException(this Logger logger, LogLevel logLevel, Exception ex, string format, params object[] arguments)
        {
            string message = string.Format(format, arguments);
            LogException(logger, logLevel, ex, message);
        }

        private static void LogException(Logger logger, LogLevel logLevel, Exception ex, string message)
        {
            // Max log level for ThreadAbortException should be Warn
            if (ex is ThreadAbortException && logLevel > LogLevel.Warn)
            {
                logLevel = LogLevel.Warn;
            }

            if (ex is CommonException)
            {
                CommonException commonEx = ex as CommonException;
                logger.Log(logLevel, "CommonException.Id = {0}", commonEx.Id);
                if (!string.IsNullOrWhiteSpace(commonEx.Reference))
                {
                    logger.Log(logLevel, "CommonException.Reference = {1}", commonEx.Reference);
                }
            }

            logger.LogException(logLevel, message, ex);

            if (ex is AggregateException)
            {
                AggregateException exAg = ex as AggregateException;
                foreach (Exception exInner in exAg.InnerExceptions)
                {
                    LogException(logger, logLevel, exInner, message);
                }
            }

            if (ex.InnerException != null)
            {
                LogException(logger, logLevel, ex.InnerException, message);
            }
        }

    }
}

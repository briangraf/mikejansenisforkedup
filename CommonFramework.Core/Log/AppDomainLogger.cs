﻿namespace CommonFramework.Log
{
    using System;
    using System.Collections.Generic;
    using NLog;

    public class AppDomainLogger
    {
        private static readonly AppDomainLogger _instance = new AppDomainLogger();
        public static AppDomainLogger Instance { get { return _instance; } }

        private static readonly Dictionary<AppDomain, Logger> Loggers = new Dictionary<AppDomain, Logger>();

        private static Logger GetLogger()
        {
            Logger logger;
            if (!Loggers.TryGetValue(AppDomain.CurrentDomain, out logger))
            {
                logger = LogManager.GetCurrentClassLogger();
                Loggers[AppDomain.CurrentDomain] = logger;
            }
            return logger;
        }

        public void InstallUnhandledExceptionLogger(AppDomain appDomain)
        {
            appDomain.UnhandledException += LogUnhandledException;
        }

        public void InstallUnhandledExceptionLogger()
        {
            GetLogger().Info("Installing Unhandled Exception Logger for AppDomain {0}", AppDomain.CurrentDomain.FriendlyName);
            InstallUnhandledExceptionLogger(AppDomain.CurrentDomain);
        }

        private void LogUnhandledException(object sender, UnhandledExceptionEventArgs e)
        {
            Logger logger = GetLogger();

            logger.Error("Unhandled AppDomain exception for {0}, Run-time is terminating: {1}, Exception type: {2}",
                AppDomain.CurrentDomain.FriendlyName, e.IsTerminating, e.ExceptionObject.GetType().FullName);
            if (e.ExceptionObject is Exception)
            {
                Exception ex = e.ExceptionObject as Exception;
                logger.FullException(e.IsTerminating ? LogLevel.Fatal : LogLevel.Error, ex, "Unhandled AppDomain exception");
            }
        }

    }
}

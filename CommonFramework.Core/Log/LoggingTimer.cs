﻿namespace CommonFramework.Log
{
    using System;
    using NLog;

    public class LoggingTimer: IDisposable
    {
        private readonly DateTime _start;
        private readonly Logger _logger;
        private readonly LogLevel _logLevel;
        private readonly string _message;

        public LoggingTimer(Logger logger, LogLevel logLevel, string format, params object[] args)
        {
            _logger = logger;
            _logLevel = logLevel;
            _message = string.Format(format, args);
            _start = DateTime.UtcNow;
            logger.Log(logLevel, "START: " + _message);
        }

        public void Dispose()
        {
            TimeSpan span = DateTime.UtcNow - _start;
            string message = string.Format("FINISH: {0} @ {1}", _message, span);
            _logger.Log(_logLevel, message);
        }
    }
}

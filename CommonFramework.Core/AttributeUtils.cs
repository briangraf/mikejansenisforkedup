﻿namespace CommonFramework
{
    using System;
    using System.Collections.Generic;
    using System.Linq;

    public static class AttributeUtils
    {
        #region Private Fields

        private static readonly Dictionary<Type, Dictionary<Type, Attribute>> AttributeMap =
            new Dictionary<Type, Dictionary<Type, Attribute>>();

        #endregion

        #region Public Methods

        public static TAttrib GetAttributeInstance<TAttrib, TClass>(bool allowInherit)
            where TAttrib : Attribute
            where TClass : class
        {
            return GetAttributeInstance(allowInherit, typeof(TClass), typeof(TAttrib)) as TAttrib;
        }

        public static TAttrib GetAttributeInstance<TAttrib>(bool allowInherit, Type classType)
            where TAttrib : Attribute
        {
            return GetAttributeInstance(allowInherit, classType, typeof(TAttrib)) as TAttrib;
        }

        public static Attribute GetAttributeInstance(bool allowInherit, Type classType, Type attribType)
        {
            Dictionary<Type, Attribute> instanceMap;
            if (!AttributeMap.TryGetValue(attribType, out instanceMap))
            {
                instanceMap = new Dictionary<Type, Attribute>();
                AttributeMap[attribType] = instanceMap;
            }

            Attribute attribute;
            if (!instanceMap.TryGetValue(classType, out attribute))
            {
                attribute =
                    (Attribute)
                     classType
                     .GetCustomAttributes(attribType, allowInherit)
                     .FirstOrDefault();
                instanceMap[classType] = attribute;
            }
            return attribute;
        }

        #endregion
    }
}

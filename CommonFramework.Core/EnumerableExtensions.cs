﻿namespace CommonFramework
{
    using System.Collections.Generic;
    using System.Linq;

    public static class EnumerableExtensions
    {
        public static bool IsNullOrEmpty<T>(this IEnumerable<T> e)
        {
            return e == null || e.IsEmpty();
        }

        public static bool IsEmpty<T>(this IEnumerable<T> e)
        {
            return e.Count() == 0;
        }
    }
}

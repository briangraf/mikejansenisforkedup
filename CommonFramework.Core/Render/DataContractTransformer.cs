﻿namespace CommonFramework.Render
{
    using System.IO;
    using System.Reflection;
    using System.Runtime.Serialization;
    using System.Xml;
    using System.Xml.XPath;
    using System.Xml.Xsl;

    public class DataContractTransformer<T> where T:class
    {
        #region Static

        private static readonly DataContractSerializer _serializer = new DataContractSerializer(typeof(T));
        private static readonly Transformer _transformer = new Transformer();

        #endregion

        #region Public Methods

        public void Transform(Assembly resourceAssembly, string resourcePath, T input, Stream result, XsltArgumentList arguments)
        {
            IXPathNavigable inputXml = Serialize(input);
            _transformer.Transform(resourceAssembly, resourcePath, inputXml, result, arguments);
        }

        public string TransformToString(Assembly resourceAssembly, string resourcePath, T input, XsltArgumentList arguments)
        {
            IXPathNavigable inputXml = Serialize(input);
            return _transformer.TransformToString(resourceAssembly, resourcePath, inputXml, arguments);
        }

        #endregion

        #region Private Methods

        private IXPathNavigable Serialize(T input)
        {
            using (MemoryStream stream = new MemoryStream())
            {
                _serializer.WriteObject(stream, input);
                stream.Flush();
                stream.Seek(0, SeekOrigin.Begin);
                XmlDocument document = new XmlDocument();
                document.Load(stream);
                return document.CreateNavigator();
            }
        }

        #endregion
    }
}

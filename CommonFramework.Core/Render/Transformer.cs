﻿namespace CommonFramework.Render
{
    using System.IO;
    using System.Reflection;
    using System.Xml;
    using System.Xml.XPath;
    using System.Xml.Xsl;

    public class Transformer
    {
        private static readonly XsltArgumentList EmptyXsltArguments = new XsltArgumentList();

        public XslCompiledTransform GetXslt(Assembly resourceAssembly, string resourcePath)
        {
            XslCompiledTransform xslt = new XslCompiledTransform();

            using (Stream xsltStream = resourceAssembly
                .GetManifestResourceStream(resourcePath))
            using (XmlReader xsltReader = XmlReader.Create(xsltStream))
            {
                xslt.Load(xsltReader);
            }
            return xslt;
        }

        public void Transform(Assembly resourceAssembly, string resourcePath, IXPathNavigable input, Stream result, XsltArgumentList arguments)
        {
            XslCompiledTransform xslt = GetXslt(resourceAssembly, resourcePath);
            xslt.Transform(input, arguments ?? EmptyXsltArguments, result);
        }

        public Stream TransformToStream(Assembly resourceAssembly, string resourcePath, IXPathNavigable input, XsltArgumentList arguments)
        {
            MemoryStream stream = new MemoryStream();
            Transform(resourceAssembly, resourcePath, input, stream, arguments);
            stream.Flush();
            stream.Seek(0L, SeekOrigin.Begin);
            return stream;
        }

        public string TransformToString(Assembly resourceAssembly, string resourcePath, IXPathNavigable input, XsltArgumentList arguments)
        {
            using (Stream stream = TransformToStream(resourceAssembly, resourcePath, input, arguments))
            using (TextReader reader = new StreamReader(stream))
            {
                return reader.ReadToEnd();
            }
        }
    }
}

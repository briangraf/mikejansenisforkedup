﻿namespace CommonFramework
{
    using System;
    using System.Collections.Generic;
    using System.Threading;

    public static class Retrier
    {
        public static bool Try(int maxRetries, TimeSpan retryInterval, IList<Exception> exceptions, bool throwLastException, Action action)
        {
            int retries = 0;
            for(;;)
            {
                try
                {
                    action();
                    return true;
                }
                catch (Exception e)
                {
                    exceptions.Add(e);
                }

                retries++;
                if (retries > maxRetries)
                {
                    if (throwLastException)
                    {
                        throw exceptions[exceptions.Count - 1];
                    }
                    else
                    {
                        return false;
                    }
                }
                Thread.Sleep(retryInterval);
            }
        }
    }
}

﻿namespace CommonFramework
{
    using System;

    public static class DateTimeExtensions
    {
        public static DateTime ToLocal(this DateTime dateTime, TimeZoneInfo timeZoneInfo)
        {
            if (dateTime.Kind != DateTimeKind.Utc)
            {
                dateTime = new DateTime(dateTime.Ticks, DateTimeKind.Utc);
            }
            return TimeZoneInfo.ConvertTimeFromUtc(dateTime, timeZoneInfo);
        }

        public static DateTime ToUtc(this DateTime dateTime, TimeZoneInfo timeZoneInfo)
        {
            if (dateTime.Kind != DateTimeKind.Unspecified)
            {
                dateTime = new DateTime(dateTime.Ticks, DateTimeKind.Unspecified);
            }
            return TimeZoneInfo.ConvertTimeToUtc(dateTime, timeZoneInfo);
        }
    }
}

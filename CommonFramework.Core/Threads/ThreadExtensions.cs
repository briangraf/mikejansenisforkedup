﻿namespace CommonFramework.Threads
{
    using System.Threading;

    public static class ThreadExtensions
    {
        public static bool IsNullOrEnded(this Thread thread)
        {
            return thread == null || thread.ThreadState == ThreadState.Aborted || thread.ThreadState == ThreadState.Stopped;
        }
    }
}

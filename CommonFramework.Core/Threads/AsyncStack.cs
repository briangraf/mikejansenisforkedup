﻿namespace CommonFramework.Threads
{
    using System;
    using System.Collections.Generic;
    using System.Collections.Immutable;
    using System.Linq;
    using System.Runtime.Remoting.Messaging;

    public class AsyncStack: IAsyncStack
    {
        private static readonly string ContextKey = "CommonFramework.Threads.AsyncStack.Key";

        private static ImmutableStack<string> CurrentContext
        {
            get
            {
                ImmutableStack<string> stack = CallContext.LogicalGetData(ContextKey) as ImmutableStack<string>;
                return stack ?? ImmutableStack.Create<string>();
            }

            set
            {
                CallContext.LogicalSetData(ContextKey, value);
            }
        }

        public IDisposable Push(string descriptor)
        {
            CurrentContext = CurrentContext.Push(descriptor);
            return new StackPopper(this);
        }

        public void Pop()
        {
            CurrentContext = CurrentContext.Pop();
        }

        public IEnumerable<string> Descriptors
        {
            get { return CurrentContext.Reverse(); }
        }

        public string DescriptorsString
        {
            get { return string.Join(".", CurrentContext.Reverse()); }
        }

        private sealed class StackPopper : IDisposable
        {
            private readonly AsyncStack _parent;
            private bool _isDisposed;

            public StackPopper(AsyncStack parent)
            {
                _parent = parent;
            }

            public void Dispose()
            {
                if (_isDisposed) return;
                _parent.Pop();
                _isDisposed = true;
            }
        }

    }
}

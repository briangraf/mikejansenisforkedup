﻿namespace CommonFramework.Threads
{
    using System;
    using System.Threading;
    using NLog;

    /// <summary>
    /// Use to monitor a thread to make sure it isn't non-responsive.  Thread must call Checkpoint() within the 
    /// interval specified in the constructor or a warning gets logged.
    /// </summary>
    public class ThreadMonitor: IDisposable
    {
        private static readonly Logger StaticLogger = LogManager.GetCurrentClassLogger();
        private static readonly TimeSpan DefaultInterval = new TimeSpan(0, 0, 60);
        private readonly TimeSpan _interval;
        private readonly Thread _thread;
        private readonly string _description;
        private readonly Timer _timer;
        private readonly Logger _logger;

        public ThreadMonitor(Thread thread, string description, TimeSpan interval, Logger logger)
        {
            _thread = thread;
            _description = description ?? thread.Name ?? "unknown";
            _interval = interval;
            _logger = logger ?? StaticLogger;

            _timer = new Timer(TimerHandler, null, interval, interval);

            _logger.Info("Start thread monitor for {0}-{1}", thread.ManagedThreadId, _description);
        }

        public ThreadMonitor(Logger logger)
            : this(Thread.CurrentThread, null, DefaultInterval, logger)
        {
        }

        public ThreadMonitor() :
            this(Thread.CurrentThread, Thread.CurrentThread.Name ?? "unknown", DefaultInterval, null)
        {
        }

        private void TimerHandler(object state)
        {
            _logger.Warn("No thread checkpoint for {0}-{1}", _thread.ManagedThreadId, _description);
        }

        public void Checkpoint()
        {
            _logger.Info("Thread checkpoint for {0}-{1}", _thread.ManagedThreadId, _description);
            _timer.Change(_interval, _interval);
        }

        public void Dispose()
        {
            _timer.Dispose();
        }
    }
}

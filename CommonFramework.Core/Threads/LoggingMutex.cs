﻿namespace CommonFramework.Threads
{
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using System.Text;
    using System.Threading;
    using System.Threading.Tasks;
    using NLog;

    public class LoggingMutex
    {
        private class Consumer
        {
            public Guid Key { get; set; }
            public string Description { get; set; }
            public DateTime WaitDateTime { get; set; }
            public DateTime? OwnDateTime { get; set; }
            public Task LogTask { get; set; }
            public CancellationTokenSource CancellationTokenSource { get; set; }
            public CancellationToken CancellationToken { get; set; }
            public Logger Logger { get; set; }
        }

        private static readonly Logger Logger = LogManager.GetCurrentClassLogger();

        private readonly SemaphoreSlim _mutex = new SemaphoreSlim(1);

        private readonly Dictionary<Guid, Consumer> _consumers = new Dictionary<Guid,Consumer>();
        private readonly Logger _logger;
        private readonly string _name;
        private readonly int _waitLogIntervalMilliseconds;

        private readonly LogLevel _waitLogLevel = LogLevel.Info;
        private readonly LogLevel _delayLogLevel = LogLevel.Warn;
        private readonly LogLevel _ownLogLevel = LogLevel.Info;
        private readonly LogLevel _releaseLogLevel = LogLevel.Info;

        public LoggingMutex(string name, int waitLogIntervalMilliseconds, Logger logger)
        {
            _name = name;
            _waitLogIntervalMilliseconds = waitLogIntervalMilliseconds;
            _logger = logger;
        }

        public LoggingMutex(string name, int waitLogIntervalMilliseconds) : this(name, waitLogIntervalMilliseconds, Logger) { }

        public LoggingMutex(string name) : this (name, 1000) {}

        public Guid Wait(Logger logger, string description)
        {
            Consumer consumer = AddConsumer(logger, description);
            consumer.Logger.Log(_waitLogLevel, "Waiting on mutex {0}: {1} {2}", _name, consumer.Key, consumer.Description);
            _mutex.Wait();
            RegisterOwnership(consumer);
            return consumer.Key;
        }

        private void RegisterOwnership(Consumer consumer)
        {
            consumer.OwnDateTime = DateTime.UtcNow;
            consumer.CancellationTokenSource.Cancel();
            consumer.Logger.Log(_ownLogLevel, "Obtained ownership on mutex {0}: {1} {2}", _name, consumer.Key, consumer.Description);
        }

        public Guid Wait(string description)
        {
            return Wait(_logger, description);
        }

        async public Task<Guid> WaitAsync(Logger logger, string description)
        {
            Consumer consumer = AddConsumer(logger, description);
            consumer.Logger.Log(_waitLogLevel, "Waiting (async) on mutex {0}: {1} {2}", _name, consumer.Key, consumer.Description);
            await _mutex.WaitAsync();
            RegisterOwnership(consumer);
            return consumer.Key;
        }

        async public Task<Guid> WaitAsync(string description)
        {
            return await WaitAsync(_logger, description);
        }

        public void Release(Guid key)
        {
            Consumer consumer = _consumers[key];
            _consumers.Remove(key);
            consumer.Logger.Log(_releaseLogLevel, "Releasing mutex {0}: {1} {2}", _name, consumer.Key, consumer.Description);
            _mutex.Release();
        }

        private Consumer AddConsumer(Logger logger, string description)
        {
            Guid key = Guid.NewGuid();
            Consumer consumer = new Consumer
                {
                    Key = key,
                    Logger = logger,
                    Description = description,
                    CancellationTokenSource = new CancellationTokenSource(),
                    WaitDateTime = DateTime.UtcNow
                };
            consumer.CancellationToken = consumer.CancellationTokenSource.Token;
            consumer.LogTask = Task.Run(async () =>
                {
                    for (; ; )
                    {
                        await Task.Delay(_waitLogIntervalMilliseconds);
                        LogWaitDelay(consumer);
                    }
                }, consumer.CancellationToken);

            _consumers.Add(consumer.Key, consumer);
            return consumer;
        }

        private void LogWaitDelay(Consumer consumer)
        {
            consumer.Logger.Log(_delayLogLevel, "Waiting on {0} mutex {1}: {2} {3}",
                _name, DateTime.UtcNow - consumer.WaitDateTime, consumer.Key, consumer.Description);

            foreach (Consumer otherConsumer in 
                                        from c in _consumers.Values
                                        where c.Key != consumer.Key
                                        orderby c.WaitDateTime
                                        select c)
            {
                StringBuilder builder = new StringBuilder();
                builder.AppendFormat(" Behind {0} {1}", otherConsumer.Key, otherConsumer.Description);
                if (otherConsumer.OwnDateTime == null)
                {
                    builder.AppendFormat(" waiting {0}", DateTime.UtcNow - otherConsumer.WaitDateTime);
                }
                else
                {
                    builder.AppendFormat(" owned {0}", DateTime.UtcNow - otherConsumer.OwnDateTime);
                }
                consumer.Logger.Log(_delayLogLevel, builder.ToString());
            }
        }


    }
}

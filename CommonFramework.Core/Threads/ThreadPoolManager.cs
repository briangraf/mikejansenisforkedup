﻿namespace CommonFramework.Threads
{
    using System;
    using System.Collections.Concurrent;
    using System.Collections.Generic;
    using System.Linq;
    using System.Reflection;
    using System.Threading;
    using NLog;
    using CommonFramework.Log;

    /// <summary>
    /// 
    /// </summary>
    public class ThreadPoolManager<T>: IDisposable
    {
        /// <summary>
        /// Tracks a worker thread and it's properties
        /// </summary>
        /// <typeparam name="T"></typeparam>
        private class WorkerThread
        {
            private readonly object _syncRoot = new object();

            private WaitHandle[] _waitHandles;

            /// <summary>
            /// Synchronization event for when the thread has been assigned an item to process
            /// </summary>
            public readonly ManualResetEvent GotItemEvent = new ManualResetEvent(false);

            /// <summary>
            /// The associated .NET thread
            /// </summary>
            public Thread Thread { get; set; }

            /// <summary>
            /// The item currently being processed
            /// </summary>
            public T Item { get; set; }

            public WaitHandle StopEvent 
            { 
                get { return _waitHandles[1]; }
                set { _waitHandles = new[] { GotItemEvent, value }; }
            }

            /// <summary>
            /// Used for the enqueuer to try and assign and item to an idle thread
            /// </summary>
            /// <param name="item"></param>
            /// <returns></returns>
            public bool TryAssignItem(T item)
            {
                lock (_syncRoot)
                {
                    if (!IsIdle)
                    {
                        return false;
                    }
                    Item = item;
                    GotItemEvent.Set();
                    return true;
                }
            }

            /// <summary>
            /// Used by the worker thread to indicate it is idle
            /// </summary>
            public void SetIdle()
            {
                lock (_syncRoot)
                {
                    Item = default(T);
                    GotItemEvent.Reset();
                }
            }

            /// <summary>
            /// Returns whether or not the worker thread is idle
            /// </summary>
            public bool IsIdle { get { return object.Equals(Item, default(T)); } }

            /// <summary>
            /// Used by the worker thread to try and wait to be assigned an item
            /// </summary>
            /// <param name="milliseconds"></param>
            /// <returns></returns>
            public bool TryWaitItem(int milliseconds)
            {
                return WaitHandle.WaitAny(_waitHandles, milliseconds) == 0 ;
            }

        }

        #region Private Fields

        private static readonly Logger logger = LogManager.GetCurrentClassLogger();

        private readonly Action<T> _itemProcessor;
        private readonly Action<T, Exception> _exceptionHandler;
        private readonly int _maxWorkerThreads;
        private readonly string _name;
        private readonly int _lowItemThreshold;
        private readonly int _idleThreadTimeoutMilliseconds;
        private readonly IComparer<T> _priorityComparer;

        private readonly ConcurrentQueue<T> _items = new ConcurrentQueue<T>();
        private readonly LinkedList<WorkerThread> _workerThreads = new LinkedList<WorkerThread>();
        private readonly object _workerThreadListSync = new object();
        private readonly object _itemsSync = new object();
        private readonly ManualResetEvent _stoppingEvent = new ManualResetEvent(false);

        private bool _disposed;
        private bool _isKnownEmpty;

        #endregion

        #region Constructors

        protected ThreadPoolManager(string name, int maxWorkerThreads, int lowItemThreshold, int idleThreadTimeoutMilliseconds, IComparer<T> priorityComparer)
        {
            MethodBase method = MethodBase.GetCurrentMethod();
            logger.Enter(method);

            try
            {
                _name = name;
                _maxWorkerThreads = maxWorkerThreads;
                _lowItemThreshold = lowItemThreshold;
                _idleThreadTimeoutMilliseconds = idleThreadTimeoutMilliseconds;
                _itemProcessor = null;
                _exceptionHandler = null;
                _priorityComparer = priorityComparer;
            }
            catch (Exception ex)
            {
                logger.ErrorException("Unhandled exception", ex);
                throw;
            }
            finally
            {
                logger.Exit(method);
            }
        }

        public ThreadPoolManager(string name, Action<T> itemProcessor, Action<T, Exception> exceptionHandler, int maxWorkerThreads)
        {
            MethodBase method = MethodBase.GetCurrentMethod();
            logger.Enter(method);

            try
            {
                _name = name;
                _itemProcessor = itemProcessor;
                _exceptionHandler = exceptionHandler;
                _maxWorkerThreads = maxWorkerThreads;
                _lowItemThreshold = maxWorkerThreads / 2;
                _idleThreadTimeoutMilliseconds = 10000;
                _priorityComparer = null;

            }
            catch (Exception ex)
            {
                logger.ErrorException("Unhandled exception", ex);
                throw;
            }
            finally
            {
                logger.Exit(method);
            }
        }

        #endregion

        #region Public Methods

        public void Enqueue(T item)
        {
            lock (_itemsSync)
            {
                EnqueueNoLock(item);
            }
        }

        public void Enqueue(IEnumerable<T> items)
        {
            lock (_itemsSync)
            {
                foreach (T item in items)
                {
                    EnqueueNoLock(item);
                }
            }
        }

        public void Dispose()
        {
            MethodBase method = MethodBase.GetCurrentMethod();
            logger.Enter(method);

            try
            {
                CheckDisposed();
                _disposed = true;
                _stoppingEvent.Set();

                while (_workerThreads.Count > 0)
                {
                    Thread thread = null;
                    lock (_workerThreadListSync)
                    {
                        if (_workerThreads.Count > 0)
                        {
                            thread = _workerThreads.First.Value.Thread;
                        }
                    }
                    if (thread != null)
                    {
                        thread.Join();
                    }
                }

                OnDispose(_items);
            }
            catch (Exception ex)
            {
                logger.ErrorException("Unhandled exception", ex);
                throw;
            }
            finally
            {
                logger.Exit(method);
            }
        }

        #endregion

        #region Private Methods

        private void EnqueueNoLock(T item)
        {
            MethodBase method = MethodBase.GetCurrentMethod();
            logger.Enter(method);

            try
            {
                CheckDisposed();
                // 1. Look for idle worker thread
                // 2. Try assigning it to idle worker thread
                // 3. If not assigned, if not at max worker threads, start a new thread with the item
                // 4. Otherwise enqueue (no idle threads, already at max threads)
                lock (_workerThreadListSync)
                {
                    WorkerThread idleWorkerThread = _workerThreads.FirstOrDefault(w => w.IsIdle);
                    bool gotAssigned = false;

                    if (idleWorkerThread != null)
                    {
                        gotAssigned = idleWorkerThread.TryAssignItem(item);
                    }

                    if (!gotAssigned)
                    {
                        _isKnownEmpty = false;
                        if (_workerThreads.Count < _maxWorkerThreads)
                        {
                            StartWorkerThread(item);
                        }
                        else
                        {
                            _items.Enqueue(item);
                            OnItemEnqueued(item);
                        }
                    }
                    else
                    {
                        OnItemAssigned(item, idleWorkerThread.Thread);
                    }
                }


            }
            catch (Exception ex)
            {
                logger.ErrorException("Unhandled exception", ex);
                throw;
            }
            finally
            {
                logger.Exit(method);
            }
        }

        private void StartWorkerThread(T item)
        {
            MethodBase method = MethodBase.GetCurrentMethod();
            logger.Enter(method);

            try
            {
                Thread workerThread = new Thread(WorkerThreadRunner);
                workerThread.Name = _name;
                WorkerThread workerThreadObject = new WorkerThread
                {
                    Thread = workerThread,
                    StopEvent = _stoppingEvent,
                    Item = item
                };
                _workerThreads.AddLast(workerThreadObject);
                workerThread.Start(workerThreadObject);

            }
            catch (Exception ex)
            {
                logger.ErrorException("Unhandled exception", ex);
                throw;
            }
            finally
            {
                logger.Exit(method);
            }
        }

        private void WorkerThreadRunner(object parameter)
        {
            MethodBase method = MethodBase.GetCurrentMethod();
            logger.Enter(method);

            WorkerThread workerThread = parameter as WorkerThread;
            OnWorkerThreadStart(workerThread.Thread);
            OnItemAssigned(workerThread.Item, workerThread.Thread);

            try
            {
                do
                {
                    T item = workerThread.Item;

                    // process current item
                    try
                    {
                        if (OnItemStart(item))
                        {
                            OnItemProcess(item);
                        }
                        OnItemComplete(item);
                    }
                    catch (Exception ex)
                    {
                        OnItemException(item, ex);
                    }

                    // Grab next item in queue
                    bool gotItem;
                    lock (_itemsSync)
                    {
                        gotItem = _items.TryDequeue(out item);
                        workerThread.Item = item;
                    }

                    // No item, go idle and wait for a while
                    if (!gotItem)
                    {
                        workerThread.SetIdle();
                        OnWorkerThreadIdle(workerThread.Thread);
                        if (!_isKnownEmpty)
                        {
                            _isKnownEmpty = true;
                            lock (_itemsSync)
                            {
                                OnQueueEmpty();
                            }
                        }
                        if (!workerThread.TryWaitItem(_idleThreadTimeoutMilliseconds))
                        {
                            break;
                        }
                    }

                    lock (_itemsSync)
                    {
                        if (_items.Count == _lowItemThreshold)
                        {
                            OnQueueLow(_items.Count);
                        }
                    }

                } while (!_stoppingEvent.WaitOne(0));

            }
            catch (Exception ex)
            {
                logger.ErrorException("Unhandled exception", ex);
                OnItemException(default(T), ex);
            }
            finally
            {
                lock (_workerThreadListSync)
                {
                    _workerThreads.Remove(workerThread);
                    OnWorkerThreadStop(workerThread.Thread);
                }
                logger.Exit(method);
            }
        }

        private void CheckDisposed()
        {
            if (_disposed) throw new ObjectDisposedException("ThreadPoolManager");
        }

        #endregion

        #region Protected Methods

        protected virtual void OnItemException(T item, Exception ex)
        {
            if (_exceptionHandler != null)
            {
                _exceptionHandler(item, ex);
            }
        }

        protected virtual bool OnItemStart(T item)
        {
            return true;
        }

        protected virtual void OnItemProcess(T item)
        {
            if (_itemProcessor != null)
            {
                _itemProcessor(item);
            }
        }

        protected virtual void OnItemComplete(T item)
        {
        }

        protected virtual void OnQueueLow(int remaining)
        {
        }

        protected virtual void OnQueueEmpty()
        {
        }

        protected virtual void OnWorkerThreadIdle(Thread thread)
        {
        }

        protected virtual void OnWorkerThreadStart(Thread thread)
        {
        }

        protected virtual void OnWorkerThreadStop(Thread thread)
        {
        }

        protected virtual void OnItemEnqueued(T item)
        {
        }

        protected virtual void OnItemAssigned(T item, Thread thread)
        {
        }

        protected virtual void OnDispose(IEnumerable<T> unprocessedItems)
        {
        }

        #endregion

    }
}

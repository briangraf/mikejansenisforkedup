﻿namespace CommonFramework.Threads
{
    using System;
    using System.Collections.Generic;

    public interface IAsyncStack
    {
        IDisposable Push(string descriptor);
        void Pop();
        IEnumerable<string> Descriptors { get; }
        string DescriptorsString { get; }
    }
}

﻿namespace CommonFramework
{
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using System.Text;
    using System.Threading.Tasks;

    public class KeyGenerator
    {
        public static readonly string AlphaNumeric = "ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz1234567890";

        public string Generator(string possibleCharacters, int length)
        {
            StringBuilder builder = new StringBuilder(length);
            for (int index = 0; index < length; length++)
            {
                builder.Append(GetRandomCharacter(possibleCharacters));
            }
            return builder.ToString();
        }

        public char GetRandomCharacter(string possibleCharacters)
        {
            return '\0';
        }
    }
}

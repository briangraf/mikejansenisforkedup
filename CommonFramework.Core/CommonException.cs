﻿namespace CommonFramework
{
    using System;
    using System.Net;
    using System.Runtime.Serialization;

    [Serializable]
    public class CommonException : ApplicationException
    {
        #region Public Properties

        public Guid Id { get; protected set; }
        public string Code { get; protected set; }
        public string Reference { get; protected set; }
        public string UserMessage { get; protected set; }
        public HttpStatusCode? RecommendedHttpStatusCode { get; protected set; }

        #endregion

        #region Constructors

        public CommonException() { }

        public CommonException(string code, string message, string userMessage, string reference, HttpStatusCode? statusCode, 
            Exception innerException) : base(message, innerException)
        {
            Code = code;
            UserMessage = userMessage;
            Reference = reference;
            RecommendedHttpStatusCode = statusCode;
            Initialize();
        }

        public CommonException(string message) : 
            this(null, message, null, null, null, null) { }

        public CommonException(string message, HttpStatusCode statusCode) : 
            this(null, message, null, null, statusCode, null) { }

        public CommonException(string message, Exception innerException) :
            this(null, message, null, null, null, innerException) { }

        public CommonException(string message, HttpStatusCode statusCode, Exception innerException) :
            this(null, message, null, null, statusCode, innerException) { }

        public CommonException(CommonExceptionBody body)
            : base(body.Message)
        {
            Id = body.Id;
            Code = body.Code;
            UserMessage = body.UserMessage;
            Reference = body.Reference;
        }

        protected CommonException(SerializationInfo info, StreamingContext context) : base(info, context) { }

        private void Initialize()
        {
            Id = Guid.NewGuid();
            if (Code == null) Code = GetType().FullName;
            if (UserMessage == null) UserMessage = Message;
        }

        #endregion

        #region Fluent API to simplify derived class constructors

        public CommonException SetCode(string code)
        {
            Code = code;
            return this;
        }

        public CommonException SetReference(string reference)
        {
            Reference = reference;
            return this;
        }

        public CommonException SetUserMessage(string userMessage)
        {
            UserMessage = userMessage;
            return this;
        }

        public CommonException SetHttpStatusCode(HttpStatusCode code)
        {
            RecommendedHttpStatusCode = code;
            return this;
        }

        #endregion

        #region Public Methods

        public CommonExceptionBody GetBody()
        {
            return new CommonExceptionBody
            {
                Id = Id,
                Code = Code,
                Message = Message,
                UserMessage = UserMessage,
                Reference = Reference
            };
        }

        public override string ToString()
        {
            return string.Format(
                "{{ \"Id\": \"{0}, \"Code\": \"{1}, \"Message\": \"{2}, \"UserMessage\": \"{3}, \"Reference\": \"{4} }}",
                Id, Code, Message, UserMessage, Reference);
        }

        #endregion

    }
}

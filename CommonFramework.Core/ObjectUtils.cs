﻿namespace CommonFramework
{
    using System.IO;
    using Newtonsoft.Json;

    public static class ObjectUtils
    {
        public static T DeepCopyViaSerialization<T>(T source) where T : class
        {
            JsonSerializer serializer = JsonSerializer.Create(new JsonSerializerSettings
            {
                DateTimeZoneHandling = DateTimeZoneHandling.Utc,
                ReferenceLoopHandling = ReferenceLoopHandling.Ignore/*,
                    TypeNameHandling = TypeNameHandling.All*/
            });

            using (StringWriter writer = new StringWriter())
            {
                serializer.Serialize(writer, source);
                using (StringReader reader = new StringReader(writer.ToString()))
                {
                    return serializer.Deserialize(reader, typeof(T)) as T;
                }
            }
        }

    }
}

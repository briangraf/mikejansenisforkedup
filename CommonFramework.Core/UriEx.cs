﻿namespace CommonFramework
{
    using System;
    using System.Collections.Generic;
    using System.Text;
    using System.Web;

    public class UriEx
    {
        public string Scheme { get; set; }
        public string Host { get; set; }
        public int Port { get; set; }
        public bool IsDefautPort { get; set; }
        public string Path { get; set; }
        public Dictionary<string,string> QueryString { get { return _queryString; } }

        private readonly Dictionary<string,string> _queryString = new Dictionary<string,string>();

        public UriEx(Uri baseUri)
        {
            Scheme = baseUri.Scheme;
            Host = baseUri.Host;
            Port = baseUri.Port;
            IsDefautPort = baseUri.IsDefaultPort;
            
            string[] parts = baseUri.PathAndQuery.Split('?');
            Path = parts[0];
            if (parts.Length > 1)
            {
                string[] wholeParams = parts[1].Split('&');
                foreach (string wholeParam in wholeParams)
                {
                    string[] keyValue = wholeParam.Split('=');
                    QueryString[keyValue[0]] = keyValue.Length > 1 ? HttpUtility.UrlDecode(keyValue[1]) : null;
                }
            }
        }

        private string GetQueryString()
        {
            if (QueryString.Count == 0) return string.Empty;

            StringBuilder builder = new StringBuilder();
            foreach (KeyValuePair<string, string> parameter in QueryString)
            {
                builder.Append(builder.Length == 0 ? '?' : '&');
                builder.Append(parameter.Key);
                if (parameter.Value != null)
                {
                    builder.Append('=');
                    builder.Append(HttpUtility.UrlEncode(parameter.Value));
                }
            }
            return builder.ToString();
        }

        public Uri ToUri()
        {
            UriBuilder builder = new UriBuilder(Scheme, Host, Port, Path, GetQueryString());
            return builder.Uri;
        }

        public override string ToString()
        {
            return ToUri().ToString();
        }

    }
}

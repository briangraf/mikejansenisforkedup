﻿namespace CommonFramework
{
    using System;

    public abstract class KeyedDatedTokenBase : TokenBase
    {
        protected override bool IsKeyed { get { return true; } }

        public KeyedDatedTokenBase(string token)
            : base(token)
        {
            if (CreatedOn == null)
            {
                CreatedOn = DateTime.UtcNow;
            }
        }

        public KeyedDatedTokenBase() { }

        [Key("$D")]
        public DateTime? CreatedOn { get; set; }

        public void Renew()
        {
            CreatedOn = DateTime.UtcNow;
            Initialize();
        }

        public bool IsExpired(TimeSpan timeout)
        {
            return (DateTime.UtcNow - CreatedOn) > timeout;
        }

        public static string Renew<T>(string token) where T:KeyedDatedTokenBase,new()
        {
            T t = new T();
            t.Initialize(token);
            t.Renew();
            return t.ToString();
        }
    }
}

﻿namespace CommonFramework
{
    using System;

    public static class NullableUtils
    {
        public static bool HasAValue<T>(this Nullable<T> obj) where T:struct
        {
            return obj != null && obj.HasValue;
        }

        public static T ValueOrDefault<T>(this Nullable<T> obj) where T : struct
        {
            return obj != null && obj.HasAValue() ? obj.Value : default(T);
        }

    }
}

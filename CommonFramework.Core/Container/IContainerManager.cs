﻿using System;
using System.Collections.Generic;

namespace CommonFramework.Container
{
    /// <summary>
    /// Abstraction for IoC/DI containers (abstracting the abstraction)
    /// </summary>
    public interface IContainerManager: IDisposable
    {
        T GetInstance<T>();
        T GetInstanceWithParams<T>(params object[] parameters);
        object GetInstance(Type type);
        object GetInstanceWithParams(Type type, params object[] parameters);
        T TryGetInstance<T>();
        object TryGetInstance(Type type);
        T GetInstance<T>(string key);
        object GetInstance(Type type, string key);
        T TryGetInstance<T>(string key);
        object TryGetInstance(Type type, string key);
        IEnumerable<T> GetAll<T>();
        IEnumerable<object> GetAll(Type type);
        void Bind(Type iface, Type implementation);
        void Bind(Type iface, object instance);
        void Bind<TIface, TImpl>()
            where TIface : class
            where TImpl : class,TIface;
        void Bind<T>(T instance) where T : class;
        void Bind<TIface, TImpl>(string key)
            where TIface : class
            where TImpl : class,TIface;
        void Release(object instance);
        void Unbind<T>();
    }
}

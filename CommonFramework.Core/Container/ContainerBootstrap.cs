﻿namespace CommonFramework.Container
{
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using System.Reflection;

    public enum ContainerState { NotInitialized, Initialized, FailedInitialization, Disposed }

    public static class ContainerBootstrap
    {
        #region Private Fields

        private static readonly object _syncRoot = new object();
        private static ContainerState _state = ContainerState.NotInitialized;
        private static IContainerManager _containerManager;

        #endregion

        #region Public Properties

        public static IContainerManager Manager { get { CheckState(false); return _containerManager; } }
        public static ContainerState State { get { return _state; } }

        #endregion

        #region Public Methods

        public static IContainerManager Initialize(IContainerManager containerManager, ContainerInitializationOptions options)
        {
            lock (_syncRoot)
            {
                CheckState(true);

                _containerManager = containerManager;
                if (!options.IsPlain)
                {
                    FindCommonBindersAndBind(_containerManager);
                }

                // Register the container manager in itself
                _containerManager.Bind<IContainerManager>(_containerManager);
                _state = ContainerState.Initialized;
            }

            return _containerManager;

        }

        public static IContainerManager Initialize(IContainerManager containerManager)
        {
            return Initialize(containerManager, ContainerInitializationOptions.DefaultOptions);
        }

        public static void Dispose()
        {
            if (_containerManager != null)
            {
                lock (_syncRoot)
                {
                    try
                    {
                        _containerManager.Dispose();
                    }
                    catch 
                    { 
                        // TODO: Log this
                    }
                    _containerManager = null;
                    _state = ContainerState.Disposed;
                }
            }

        }

        #endregion

        #region Private Methods

        private static void FindCommonBindersAndBind(IContainerManager cm)
        {
            IEnumerable<ICommonBinder> binders = ReflectionUtils.GetImplementationInstances<ICommonBinder>();
            foreach (ICommonBinder binder in binders)
            {
                binder.Bind(cm);
            }
        }

        private static void CheckState(bool initializing)
        {
            switch (_state)
            {
                case ContainerState.NotInitialized:
                    if (!initializing) throw new ContainerNotInitializedException();
                    break;
                case ContainerState.Initialized:
                    if (initializing) throw new ContainerAlreadyInitializedException();
                    break;
                case ContainerState.FailedInitialization:
                    throw new ContainerFailedToInitializeException();
                default:
                    throw new ContainerDisposedException();
            }
        }

        #endregion
    }
}

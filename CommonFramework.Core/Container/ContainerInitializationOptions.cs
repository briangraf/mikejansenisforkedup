﻿using System;

namespace CommonFramework.Container
{
    public class ContainerInitializationOptions
    {
        public static readonly ContainerInitializationOptions DefaultOptions = new ContainerInitializationOptions
        {
            IsPlain = false
        };

        public bool IsPlain { get; set; }
    }
}
 

﻿namespace CommonFramework.Container.Ninject
{
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using CommonFramework.Container;
    using global::Ninject;
    using global::Ninject.Parameters;
    using global::Ninject.Planning.Bindings;

    /// <summary>
    /// Container Manager for Ninject
    /// </summary>
    public class NinjectContainerManager: IContainerManager
    {
        #region Private Fields

        private readonly IKernel _kernel;

        #endregion

        #region Constructors

        public NinjectContainerManager(IKernel kernel)
        {
            _kernel = kernel;
        }

        public NinjectContainerManager() : this(new StandardKernel()) { }

        #endregion

        #region Static Methods

        public static IContainerManager Initialize(ContainerInitializationOptions options)
        {
            IContainerManager cm = new NinjectContainerManager();
            return ContainerBootstrap.Initialize(cm, options);
        }

        public static IContainerManager Initialize()
        {
            return Initialize(ContainerInitializationOptions.DefaultOptions);
        }

        #endregion

        #region IContainer Manager implementation

        public T GetInstance<T>()
        {
            return _kernel.Get<T>();
        }

        public T GetInstanceWithParams<T>(params object[] parameters)
        {
            IParameter[] args = GetConstructorArguments(parameters);
            return _kernel.Get<T>(args);
        }

        public object GetInstance(Type type)
        {
            return _kernel.Get(type);
        }

        public object GetInstanceWithParams(Type type, params object[] parameters)
        {
            IParameter[] args = GetConstructorArguments(parameters);
            return _kernel.Get(type, args);
        }

        public T TryGetInstance<T>()
        {
            return _kernel.TryGet<T>();
        }

        public object TryGetInstance(Type type)
        {
            return _kernel.TryGet(type);
        }

        public T GetInstance<T>(string key)
        {
            return _kernel.Get<T>(key);
        }

        public object GetInstance(Type type, string key)
        {
            return _kernel.Get(type, key);
        }

        public T TryGetInstance<T>(string key)
        {
            return _kernel.TryGet<T>(key);
        }

        public object TryGetInstance(Type type, string key)
        {
            return _kernel.TryGet(type, key);
        }

        public IEnumerable<T> GetAll<T>()
        {
            return _kernel.GetAll<T>();
        }

        public IEnumerable<object> GetAll(Type type)
        {
            return _kernel.GetAll(type);
        }

        public void Bind(Type iface, Type implementation)
        {
            _kernel.Bind(iface).To(implementation);
        }

        public void Bind(Type iface, object instance)
        {
            _kernel.Bind(iface).ToConstant(instance);
        }

        public void Bind<TIface, TImpl>()
            where TIface : class
            where TImpl : class, TIface
        {
            _kernel.Bind<TIface>().To<TImpl>();
        }

        public void Bind<T>(T instance) where T : class
        {
            _kernel.Bind<T>().ToConstant(instance);
        }

        public void Bind<TIface, TImpl>(string key)
            where TIface : class
            where TImpl : class, TIface
        {
            _kernel.Bind<TIface>().To<TImpl>().Named(key);
        }

        public void Release(object instance)
        {
            _kernel.Release(instance);
        }

        public void Unbind<T>()
        {
            _kernel.GetBindings(typeof(T))
                .ToList()
                .ForEach(b => _kernel.RemoveBinding(b));
        }

        public void Dispose()
        {
            _kernel.Dispose();
        }

        #endregion

        #region Private Methods

        private static IParameter[] GetConstructorArguments(object[] parameters)
        {
            List<ConstructorArgument> args = new List<ConstructorArgument>();
            for (int index = 0; index < parameters.Length; index += 2)
            {
                ConstructorArgument arg = new ConstructorArgument(parameters[index] as string, parameters[index + 1]);
                args.Add(arg);
            }
            return args.Select(a => (IParameter)a).ToArray();
        }

        #endregion

    }
}

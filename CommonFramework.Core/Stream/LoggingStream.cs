﻿namespace CommonFramework.Stream
{
    using System;
    using System.IO;
    using NLog;
    using CommonFramework.Log;

    public delegate void LoggingStreamEventHandler(LoggingStream stream);

    [Flags]
    public enum LoggingStreamOptions
    {
        AsText = 0x00000001,
        LogReads = 0x00000002,
        LogWrites = 0x00000004
    }

    public class LoggingStream: FilterStreamBase
    {
        private readonly string _description;
        private readonly Logger _logger;
        private readonly LogLevel _logLevel;
        private readonly bool _asText;
        private readonly bool _logReads;
        private readonly bool _logWrites;
        private readonly MemoryStream _streamForText;
        private readonly bool _isEnabled;

        public string Description { get { return _description; } }
        public Logger Logger { get { return _logger; } }
        public LogLevel LogLevel { get { return _logLevel; } }

        public event LoggingStreamEventHandler Closed;

        public LoggingStream(Stream stream, Logger logger, LogLevel logLevel, string description, LoggingStreamOptions options)
            : base(stream)
        {
            _logger = logger;
            _description = description;
            _asText = (options & LoggingStreamOptions.AsText) == LoggingStreamOptions.AsText;
            _logReads = (options & LoggingStreamOptions.LogReads) == LoggingStreamOptions.LogReads;
            _logWrites = (options & LoggingStreamOptions.LogWrites) == LoggingStreamOptions.LogWrites;
            _isEnabled = logger.IsEnabled(logLevel);
            _streamForText = _asText && _isEnabled ? new MemoryStream() : null;
            _logLevel = logLevel;
        }

        public override void Flush()
        {
            if (_isEnabled)
            {
                _logger.Log(_logLevel, "Flush stream: {0}", _description);
            }
            base.Flush();
        }

        public override long Position
        {
            get
            {
                return base.Position;
            }
            set
            {
                if (_isEnabled)
                {
                    _logger.Log(_logLevel, "Set stream position to {0}: {1}", value, _description);
                }
                base.Position = value;
            }
        }

        public override long Seek(long offset, SeekOrigin origin)
        {
            if (_isEnabled)
            {
                _logger.Log(_logLevel, "Seek stream position to {0} {1}: {2}", origin, offset, _description);
            }
            return base.Seek(offset, origin);
        }

        public override void SetLength(long value)
        {
            if (_isEnabled)
            {
                _logger.Log(_logLevel, "Set stream length to {0}: {1}", value, _description);
            }
            base.SetLength(value);
        }

        public override void Write(byte[] buffer, int offset, int count)
        {
            if (_isEnabled && _logWrites)
            {
                if (_asText)
                {
                    _logger.Log(_logLevel, "Write to stream {0} bytes: {1}", count, _description);
                    _streamForText.Write(buffer, offset, count);
                }
                else
                {
                    _logger.WriteBuffer(_logLevel, buffer, offset, count, _description);
                }
            }

            base.Write(buffer, offset, count);
        }

        public override int Read(byte[] buffer, int offset, int count)
        {
            int bytesRead = base.Read(buffer, offset, count);

            if (_isEnabled && _logReads)
            {
                if (_asText)
                {
                    _logger.Log(_logLevel, "Read from stream {0} bytes: {1}", bytesRead, _description);
                    _streamForText.Write(buffer, offset, count);
                }
                else
                {
                    _logger.WriteBuffer(_logLevel, buffer, offset, count, _description);
                }
            }
            return bytesRead;
        }

        public override void Close()
        {
            if (_isEnabled && _asText)
            {
                _streamForText.Flush();
                _streamForText.Position = 0L;
                using (StreamReader reader = new StreamReader(_streamForText))
                {
                    _logger.Log(_logLevel, "Stream text: {0}\n{1}", _description, reader.ReadToEnd());
                }
                _streamForText.Dispose();

            }
            base.Close();

            LoggingStreamEventHandler handler = Closed;
            if (handler != null)
            {
                handler(this);
            }
        }
    }
}

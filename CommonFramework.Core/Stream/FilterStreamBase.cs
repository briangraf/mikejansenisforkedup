﻿namespace CommonFramework.Stream
{
    using System.IO;

    public abstract class FilterStreamBase : Stream
    {
        private readonly Stream _originalStream;

        protected Stream OriginalStream { get { return _originalStream; } }

        protected FilterStreamBase(Stream stream)
        {
            _originalStream = stream;
        }

        public override bool CanRead
        {
            get { return _originalStream.CanRead; }
        }

        public override bool CanSeek
        {
            get { return _originalStream.CanSeek; }
        }

        public override bool CanWrite
        {
            get { return _originalStream.CanWrite; }
        }

        public override void Flush()
        {
            _originalStream.Flush();
        }

        public override void Close()
        {
            _originalStream.Close();
        }

        public override long Length
        {
            get { return _originalStream.Length; }
        }

        public override long Position
        {
            get
            {
                return _originalStream.Position;
            }
            set
            {
                _originalStream.Position = value;
            }
        }

        public override int Read(byte[] buffer, int offset, int count)
        {
            return _originalStream.Read(buffer, offset, count);
        }

        public override long Seek(long offset, SeekOrigin origin)
        {
            return _originalStream.Seek(offset, origin);
        }

        public override void SetLength(long value)
        {
            _originalStream.SetLength(value);
        }

        public override void Write(byte[] buffer, int offset, int count)
        {
            _originalStream.Write(buffer, offset, count);
        }
    }
}

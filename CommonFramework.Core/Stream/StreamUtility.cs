﻿namespace CommonFramework.Stream
{
    using System;
    using System.IO;
    using System.Reflection;
    using NLog;
    using CommonFramework.Log;

    /// <summary>
    /// Various Stream related helper methods
    /// </summary>
    public static class StreamUtility
    {
        private static readonly Logger staticLogger = LogManager.GetCurrentClassLogger();

        /// <summary>
        /// Creates a TextReader for the specified stream.  If the specified logging category is configured for
        /// Verbose, a TeeReader is returned that both acts as a normal reader off the specified stream and also
        /// tees all data read to a LoggingWriter.
        /// </summary>
        /// <param name="stream"></param>
        /// <param name="loggingCategory"></param>
        /// <param name="description"></param>
        /// <returns></returns>
        public static TextReader CreateTextReader(this Stream stream, Logger logger, string description)
        {
            if (logger.IsDebugEnabled)
            {
                return new TeeReader(new StreamReader(stream), new LoggingWriter(logger, description));
            }
            else
            {
                return new StreamReader(stream);
            }
        }

        /// <summary>
        /// Creates a TextReader for the specified stream.  If the Streaming logging category is configured for
        /// Verbose, a TeeReader is returned that both acts as a normal reader off the specified stream and also
        /// tees all data read to a LoggingWriter.
        /// </summary>
        /// <param name="stream"></param>
        /// <returns></returns>
        public static TextReader CreateTextReader(this Stream stream)
        {
            if (staticLogger.IsDebugEnabled)
            {
                return new TeeReader(new StreamReader(stream), new LoggingWriter());
            }
            else
            {
                return new StreamReader(stream);
            }
        }

        /// <summary>
        /// Creates a TextWriter for the specified stream.  If the specified logging category is configured for
        /// Verbose, a TeeWriter is returned that both acts as a normal writer off the specified stream and also
        /// tees all data written to a LoggingWriter.
        /// </summary>
        /// <param name="stream"></param>
        /// <param name="loggingCategory"></param>
        /// <param name="description"></param>
        /// <returns></returns>
        public static TextWriter CreateTextWriter(this Stream stream, Logger logger, string description)
        {
            if (logger.IsDebugEnabled)
            {
                return new TeeWriter(new StreamWriter(stream), new LoggingWriter(logger, description));
            }
            else
            {
                return new StreamWriter(stream);
            }
        }

        /// <summary>
        /// Creates a TextWriter for the specified stream.  If the Streaming logging category is configured for
        /// Verbose, a TeeWriter is returned that both acts as a normal writer off the specified stream and also
        /// tees all data written to a LoggingWriter.
        /// </summary>
        /// <param name="stream"></param>
        /// <returns></returns>
        public static TextWriter CreateTextWriter(this Stream stream)
        {
            if (staticLogger.IsDebugEnabled)
            {
                return new TeeWriter(new StreamWriter(stream), new LoggingWriter());
            }
            else
            {
                return new StreamWriter(stream);
            }
        }

        /// <summary>
        /// Transfer an input stream to an output stream
        /// </summary>
        /// <param name="inputStream">the input stream</param>
        /// <param name="outputStream">the output stream</param>
        /// <param name="blockSize">size of each read block</param>
        /// <returns>Total number of bytes transferred</returns>
        public static long TransferStream(this Stream inputStream, Stream outputStream, int blockSize)
        {
            MethodBase currentMethod = MethodBase.GetCurrentMethod();
            staticLogger.Enter(currentMethod);

            try
            {
                // ********************************************************************

                long totalBytesTransferred = 0L;

                byte[] transferBuffer = new byte[blockSize];
                int bytesRead = inputStream.Read(transferBuffer, 0, blockSize);
                while (bytesRead > 0)
                {
                    staticLogger.WriteBuffer(LogLevel.Debug, transferBuffer, 0, bytesRead,
                        "TransferStream");
                    totalBytesTransferred += bytesRead;
                    outputStream.Write(transferBuffer, 0, bytesRead);
                    bytesRead = inputStream.Read(transferBuffer, 0, blockSize);
                }

                return totalBytesTransferred;

                // ********************************************************************
            }
            catch (Exception ex)
            {
                staticLogger.ErrorException("Unhandled exception", ex);
                throw;
            }
            finally
            {
                staticLogger.Exit(currentMethod);
            }

        }

        public delegate bool ForEachBlockCallback(byte[] buffer, int blockLength, bool isEof);

        public static void ForEachBlock(this Stream stream, byte[] buffer, bool isRewind, ForEachBlockCallback callback)
        {
            long position = isRewind ? stream.Position : 0L;
            int bufferLength = buffer.Length;
            int readLength;
            bool isMoreData = true;

            while(isMoreData)
            {
                readLength = stream.Read(buffer, 0, bufferLength);
                isMoreData = readLength == bufferLength;
                if (!callback(buffer, readLength, !isMoreData))
                {
                    break;
                }
            }

            if (isRewind)
            {
                stream.Position = position;
            }
        }

    }
}

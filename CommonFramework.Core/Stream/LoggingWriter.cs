﻿namespace CommonFramework.Stream
{
    using System;
    using System.IO;
    using NLog;

    /// <summary>
    /// A derivative of StringWriter that logs its contents when the writer is closed.  Used primarily by the Framework in combination 
    /// with TeeReader/TeeWriter to debug text streams.
    /// </summary>
    public class LoggingWriter : StringWriter
    {
        private static readonly Logger staticLogger = LogManager.GetCurrentClassLogger();

        private readonly Logger _logger;
        private readonly string _description;

        public LoggingWriter()
            : this(staticLogger, "Text Stream")
        {
        }

        public LoggingWriter(Logger logger)
            : this(logger, "Text Stream")
        {
        }

        public LoggingWriter(Logger logger, string description)
        {
            _logger = logger;
            _description = description;
        }

        #region Public Methods

        /// <summary>
        /// Closes the writer at which point its contents are logged.
        /// </summary>
        public override void Close()
        {
            base.Close();
            _logger.Debug("{1}:{0}{2}", Environment.NewLine, _description, GetStringBuilder());
        }

        #endregion

    }
}

﻿namespace CommonFramework.Stream
{
    using System;
    using System.IO;

    public class NotifyingStream: FilterStreamBase
    {
        public NotifyingStream(Stream stream) : base(stream) { }

        public event Action OnClose;
        public event Action OnDispose;

        public override void Close()
        {
            base.Close();
            Action handler = OnClose;
            if (handler != null) handler();
        }

        protected override void Dispose(bool disposing)
        {
            base.Dispose(disposing);
            Action handler = OnDispose;
            if (handler != null) handler();
        }
    }
}

﻿namespace CommonFramework.Stream
{
    using System;
    using System.IO;
    using System.Text;

    /// <summary>
    /// Wrapper for a TextWriter that writes to two TextWriter objects.
    /// </summary>
    public class TeeWriter : TextWriter
    {
        private TextWriter _writer;
        private TextWriter _teeWriter;

        /// <summary>
        /// Creates a TextWriter that tees everything written to two TextWriters
        /// </summary>
        /// <param name="writer"></param>
        /// <param name="teeWriter"></param>
        public TeeWriter(TextWriter writer, TextWriter teeWriter)
        {
            _writer = writer;
            _teeWriter = teeWriter;
        }

        public override Encoding Encoding
        {
            get { return _writer.Encoding; }
        }

        public override void Close()
        {
            _writer.Close();
            _teeWriter.Close();
        }

        protected override void Dispose(bool disposing)
        {
            _writer.Dispose();
            _teeWriter.Dispose();
        }

        public override void Flush()
        {
            _writer.Flush();
            _teeWriter.Flush();
        }

        public override IFormatProvider FormatProvider
        {
            get
            {
                return _writer.FormatProvider;
            }
        }

        public override string NewLine
        {
            get
            {
                return _writer.NewLine;
            }
            set
            {
                _writer.NewLine = value;
                _teeWriter.NewLine = value;
            }
        }

        public override string ToString()
        {
            return _writer.ToString();
        }

        public override void Write(bool value)
        {
            _writer.Write(value);
            _teeWriter.Write(value);
        }

        public override void Write(char value)
        {
            _writer.Write(value);
            _teeWriter.Write(value);
        }

        public override void Write(char[] buffer)
        {
            _writer.Write(buffer);
            _teeWriter.Write(buffer);
        }

        public override void Write(char[] buffer, int index, int count)
        {
            _writer.Write(buffer, index, count);
            _teeWriter.Write(buffer, index, count);
        }

        public override void Write(decimal value)
        {
            _writer.Write(value);
            _teeWriter.Write(value);
        }

        public override void Write(double value)
        {
            _writer.Write(value);
            _teeWriter.Write(value);
        }

        public override void Write(float value)
        {
            _writer.Write(value);
            _teeWriter.Write(value);
        }

        public override void Write(int value)
        {
            _writer.Write(value);
            _teeWriter.Write(value);
        }

        public override void Write(long value)
        {
            _writer.Write(value);
            _teeWriter.Write(value);
        }

        public override void Write(object value)
        {
            _writer.Write(value);
            _teeWriter.Write(value);
        }

        public override void Write(string format, object arg0)
        {
            _writer.Write(format, arg0);
            _teeWriter.Write(format, arg0);
        }

        public override void Write(string format, object arg0, object arg1)
        {
            _writer.Write(format, arg0, arg1);
            _teeWriter.Write(format, arg0, arg1);
        }

        public override void Write(string format, object arg0, object arg1, object arg2)
        {
            _writer.Write(format, arg0, arg1, arg2);
            _teeWriter.Write(format, arg0, arg1, arg2);
        }

        public override void Write(string format, params object[] arg)
        {
            _writer.Write(format, arg);
            _teeWriter.Write(format, arg);
        }

        public override void Write(string value)
        {
            _writer.Write(value);
            _teeWriter.Write(value);
        }

        public override void Write(uint value)
        {
            _writer.Write(value);
            _teeWriter.Write(value);
        }

        public override void Write(ulong value)
        {
            _writer.Write(value);
            _teeWriter.Write(value);
        }

        public override void WriteLine()
        {
            _writer.WriteLine();
            _teeWriter.WriteLine();
        }

        public override void WriteLine(bool value)
        {
            _writer.WriteLine(value);
            _teeWriter.WriteLine(value);
        }

        public override void WriteLine(char value)
        {
            _writer.WriteLine(value);
            _teeWriter.WriteLine(value);
        }

        public override void WriteLine(char[] buffer)
        {
            _writer.WriteLine(buffer);
            _teeWriter.WriteLine(buffer);
        }

        public override void WriteLine(char[] buffer, int index, int count)
        {
            _writer.WriteLine(buffer, index, count);
            _teeWriter.WriteLine(buffer, index, count);
        }

        public override void WriteLine(decimal value)
        {
            _writer.WriteLine(value);
            _teeWriter.WriteLine(value);
        }

        public override void WriteLine(double value)
        {
            _writer.WriteLine(value);
            _teeWriter.WriteLine(value);
        }

        public override void WriteLine(float value)
        {
            _writer.WriteLine(value);
            _teeWriter.WriteLine(value);
        }

        public override void WriteLine(int value)
        {
            _writer.WriteLine(value);
            _teeWriter.WriteLine(value);
        }

        public override void WriteLine(long value)
        {
            _writer.WriteLine(value);
            _teeWriter.WriteLine(value);
        }

        public override void WriteLine(object value)
        {
            _writer.WriteLine(value);
            _teeWriter.WriteLine(value);
        }

        public override void WriteLine(string format, object arg0)
        {
            _writer.WriteLine(format, arg0);
            _teeWriter.WriteLine(format, arg0);
        }

        public override void WriteLine(string format, object arg0, object arg1)
        {
            _writer.WriteLine(format, arg0, arg1);
            _teeWriter.WriteLine(format, arg0, arg1);
        }

        public override void WriteLine(string format, object arg0, object arg1, object arg2)
        {
            _writer.WriteLine(format, arg0, arg1, arg2);
            _teeWriter.WriteLine(format, arg0, arg1, arg2);
        }

        public override void WriteLine(string format, params object[] arg)
        {
            _writer.WriteLine(format, arg);
            _teeWriter.WriteLine(format, arg);
        }

        public override void WriteLine(string value)
        {
            _writer.WriteLine(value);
            _teeWriter.WriteLine(value);
        }

        public override void WriteLine(uint value)
        {
            _writer.WriteLine(value);
            _teeWriter.WriteLine(value);
        }

        public override void WriteLine(ulong value)
        {
            _writer.WriteLine(value);
            _teeWriter.WriteLine(value);
        }

    }
}

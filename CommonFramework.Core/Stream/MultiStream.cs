﻿namespace CommonFramework.Stream
{
    using System;
    using System.Collections.Generic;
    using System.IO;
    using NLog;
    using CommonFramework.Log;

    /// <summary>
    /// A read-only stream that concatenates a queue of streams so they
    /// appear to be one continuous stream to the consumer.
    /// </summary>
    public class MultiStream : Stream
    {
        private static Logger logger = LogManager.GetCurrentClassLogger();

        private Queue<Stream> _streams = new Queue<Stream>();
        private Stream _currentStream = null;

        /// <summary>
        /// Enqueue a stream to be read by the consumer of this stream.
        /// </summary>
        /// <param name="stream"></param>
        public void Add(Stream stream)
        {
            logger.Debug("MultiStream: Add stream length {0} of type {1}",
                stream.Length, stream.GetType().Name);
            _streams.Enqueue(stream);
        }

        public override bool CanRead
        {
            get { return true; }
        }

        public override bool CanSeek
        {
            get { return false; }
        }

        public override bool CanWrite
        {
            get { return false; }
        }

        public override void Flush()
        {
            throw new NotImplementedException();
        }

        public override long Length
        {
            get { throw new NotImplementedException(); }
        }

        public override long Position
        {
            get
            {
                throw new NotImplementedException();
            }
            set
            {
                throw new NotImplementedException();
            }
        }

        public override int Read(byte[] buffer, int offset, int count)
        {
            int bytesRead = 0;

            if (_currentStream == null)
            {
                if (_streams.Count > 0)
                {
                    _currentStream = _streams.Dequeue();
                    logger.Debug(
                        "MultiStream: Loading stream length {0} of type {1}",
                        _currentStream.Length, _currentStream.GetType().Name);
                }
            }

            if (_currentStream != null)
            {
                bytesRead = _currentStream.Read(buffer, offset, count);
                logger.WriteBuffer(LogLevel.Debug,
                    buffer, offset, bytesRead, "MultiStream: Read");
                if (bytesRead < count)
                {
                    _currentStream.Dispose();
                    _currentStream = null;
                    bytesRead += Read(buffer, offset + bytesRead, count - bytesRead);
                }
            }

            return bytesRead;
        }

        public override long Seek(long offset, SeekOrigin origin)
        {
            throw new NotImplementedException();
        }

        public override void SetLength(long value)
        {
            throw new NotImplementedException();
        }

        public override void Write(byte[] buffer, int offset, int count)
        {
            throw new NotImplementedException();
        }
    }
}

﻿namespace CommonFramework.Stream
{
    using System.IO;

    /// <summary>
    /// Wrapper for a TextReader that also writes everything read to a TextWriter
    /// </summary>
    public class TeeReader : TextReader
    {
        private TextReader _reader;
        private TextWriter _writer;

        /// <summary>
        /// Constructs a TextReader that wraps another TextReader and also tees everything read to a TextWriter
        /// </summary>
        /// <param name="reader">TextReader to wrap</param>
        /// <param name="teeWriter">TextWriter that also receives everything read from the TextReader</param>
        public TeeReader(TextReader reader, TextWriter teeWriter)
        {
            _reader = reader;
            _writer = teeWriter;
        }

        public override void Close()
        {
            _reader.Close();
            _writer.Close();
        }

        protected override void Dispose(bool disposing)
        {
            _reader.Dispose();
            _writer.Dispose();
        }

        public override int Peek()
        {
            return _reader.Peek();
        }

        public override int Read()
        {
            int read = _reader.Read();
            if (read != -1)
                _writer.Write((char)read);
            return read;
        }

        public override int Read(char[] buffer, int index, int count)
        {
            int numRead = _reader.Read(buffer, index, count);
            if (numRead > 0)
                _writer.Write(buffer, index, numRead);
            return numRead;
        }

        public override int ReadBlock(char[] buffer, int index, int count)
        {
            int numRead = _reader.ReadBlock(buffer, index, count);
            if (numRead > 0)
                _writer.Write(buffer, index, numRead);
            return numRead;
        }

        public override string ReadLine()
        {
            string line = _reader.ReadLine();
            if (line != null)
                _writer.WriteLine(line);
            return line;
        }

        public override string ReadToEnd()
        {
            string read = _reader.ReadToEnd();
            if (read != null)
                _writer.Write(read);
            return read;
        }

        public override string ToString()
        {
            return _reader.ToString();
        }
    }

}

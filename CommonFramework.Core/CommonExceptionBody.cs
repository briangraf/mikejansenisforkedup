﻿namespace CommonFramework
{
    using System;
    
    public class CommonExceptionBody
    {
        public Guid Id { get; set; }
        public string Code { get; set; }
        public string Message { get; set; }
        public string UserMessage { get; set; }
        public string Reference { get; set; }
    }
}

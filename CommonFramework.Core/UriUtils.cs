﻿namespace CommonFramework
{
    using System;

    public static class UriUtils
    {
        public static string GetUrlWithoutPort(this Uri uri)
        {
            return string.Format("{0}://{1}{2}", uri.Scheme, uri.Host, uri.PathAndQuery);
        }

        public static string GetUrlWithExtraParameters(this Uri uri, params string[] extraParameters)
        {
            UriEx uriEx = new UriEx(uri);
            for (int index = 0; index < extraParameters.Length; index += 2)
            {
                uriEx.QueryString[extraParameters[index]] = extraParameters[index + 1];
            }
            return uriEx.ToString();
        }
    }
}

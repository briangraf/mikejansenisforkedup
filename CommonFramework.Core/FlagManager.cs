﻿namespace CommonFramework
{
    using System.Collections.Generic;

    public enum FlagEntryKind
    {
        Include,
        Exclude,
        List
    }

    /// <summary>
    /// Manages flags, lists of flags, and parsing a flag string to determine what flags are set
    /// </summary>
    public class FlagManager
    {
        private char _separator = ',';
        public char Separator { get { return _separator; } set { _separator = value; } }

        private char _listPrefix = '@';
        public char ListPrefix { get { return _listPrefix; } set { _listPrefix= value; } }

        private char _includePrefix = '+';
        public char IncludePrefix { get { return _includePrefix; } set { _includePrefix= value; } }

        private char _excludePrefix = '-';
        public char ExcludePrefix { get { return _excludePrefix; } set { _excludePrefix = value; } }

        private readonly List<string> _flags = new List<string>();
        private readonly List<string> _defaultFlags = new List<string>();
        private readonly Dictionary<string, IDictionary<string,FlagEntryKind>> _lists = new Dictionary<string, IDictionary<string,FlagEntryKind>>();

        private class FlagEntry
        {
            public string Value { get; set; }
            public FlagEntryKind Kind { get; set; }
        }

        public void AddFlag(string flag)
        {
            _flags.Add(flag);
        }

        public void AddFlagWithDefault(string flag)
        {
            _flags.Add(flag);
            _defaultFlags.Add(flag);
        }

        public void AddList(string list, IDictionary<string, FlagEntryKind> flags)
        {
            _lists[list] = flags;
        }

        private IDictionary<string, FlagEntryKind> ParseFlagString(string flagString)
        {
            Dictionary<string, FlagEntryKind> flags = new Dictionary<string, FlagEntryKind>();
            string[] parts = flagString.Split(Separator);

            foreach (string part in parts)
            {
                if (part.Length > 0)
                {
                    char firstChar = part[0];
                    string remainder = part.Substring(1);
                    if (firstChar == ExcludePrefix)
                    {
                        flags[remainder] = FlagEntryKind.Exclude;
                    }
                    else if (firstChar == IncludePrefix)
                    {
                        flags[remainder] = FlagEntryKind.Include;
                    }
                    else if (firstChar == ListPrefix)
                    {
                        flags[remainder] = FlagEntryKind.List;
                    }
                    else
                    {
                        flags[part] = FlagEntryKind.Include;
                    }
                }
            }
            return flags;
        }

        public IEnumerable<string> ParseFlags(string flagString)
        {
            List<string> setFlags = new List<string>(_defaultFlags);
            IDictionary<string, FlagEntryKind> flags = ParseFlagString(flagString);
            ApplyFlagList(setFlags, flags);
            return setFlags;
        }

        private void ApplyFlagList(IList<string> setFlags, IDictionary<string, FlagEntryKind> flagsToApply)
        {
            foreach (KeyValuePair<string, FlagEntryKind> flagToApply in flagsToApply)
            {
                switch (flagToApply.Value)
                {
                    case FlagEntryKind.Include:
                        if (!setFlags.Contains(flagToApply.Key))
                        {
                            setFlags.Add(flagToApply.Key);
                        }
                        break;
                    case FlagEntryKind.Exclude:
                        setFlags.Remove(flagToApply.Key);
                        break;
                    case FlagEntryKind.List:
                        IDictionary<string, FlagEntryKind> list;
                        if (_lists.TryGetValue(flagToApply.Key, out list))
                        {
                            ApplyFlagList(setFlags, list);
                        }
                        break;
                }
               
            }
        }
    }
}

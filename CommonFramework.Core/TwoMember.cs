﻿namespace CommonFramework
{
    /// <summary>
    /// Useful for two member structures used as dictionary keys, since Equals and GetHashCode have some simple overrides
    /// </summary>
    /// <typeparam name="T1"></typeparam>
    /// <typeparam name="T2"></typeparam>
    public class TwoMember<T1, T2>
    {
        public T1 M1 { get; set; }
        public T2 M2 { get; set; }

        public override bool Equals(object obj)
        {
            TwoMember<T1, T2> other = obj as TwoMember<T1, T2>;
            return
                other != null &&
                M1.Equals(other.M1) &&
                M2.Equals(other.M2);
        }

        public override int GetHashCode()
        {
            return M1.GetHashCode() ^
                    M2.GetHashCode();
        }
    }
}

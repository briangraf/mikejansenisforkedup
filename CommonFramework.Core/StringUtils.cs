﻿namespace CommonFramework
{
    using System;
    using System.Collections.Generic;
    using System.ComponentModel;
    using System.Text;

    public static class StringUtils
    {
        /// <summary>
        /// Convert a Camel-cased string into space delimited upper-cased, e.g. MyEnumValue => MY ENUM VALUE
        /// </summary>
        /// <param name="camelCased"></param>
        /// <returns></returns>
        public static string GetUpperWordsFromCamelCased(string camelCased)
        {
            StringBuilder builder = new StringBuilder();

            for (int index = 0; index < camelCased.Length; index++)
            {
                char character = camelCased[index];
                if (index == 0 || char.IsLower(character) || !char.IsLetter(character))
                {
                    builder.Append(char.ToUpperInvariant(character));
                }
                else
                {
                    builder.Append(' ');
                    builder.Append(char.ToUpperInvariant(character));
                }
            }

            return builder.ToString();
        }

        public static string Translate(this string original, string from, string to)
        {
            for (int index = 0; index < from.Length; index++)
            {
                original = original.Replace(from[index], to[index]);
            }
            return original;
        }

        public static string Duplicate(string toDuplicate, int count)
        {
            StringBuilder builder = new StringBuilder(toDuplicate.Length * count);
            for (int index = 0; index < count; index++) builder.Append(toDuplicate);
            return builder.ToString();
        }

        public static IList<string> CreateIndentLevels(string indent, int maximumLevels)
        {
            List<string> levels = new List<string>(maximumLevels);
            StringBuilder builder = new StringBuilder();
            for (int level = 0; level < maximumLevels; level++)
            {
                levels.Add(builder.ToString());
                builder.Append(indent);
            }
            return levels;
        }

        public static string CleanUsPhoneNumber(string uncleanedPhoneNumber, string invalidResult = null)
        {
            StringBuilder cleaned = new StringBuilder();
            foreach (char c in uncleanedPhoneNumber)
            {
                if (char.IsDigit(c))
                {
                    cleaned.Append(c);
                }
            }

            string cleanedString = cleaned.ToString();
            if (cleanedString.Length == 11 && cleanedString[0] == '1')
            {
                cleanedString = cleanedString.Substring(1);
            }

            if (cleanedString.Length != 10)
            {
                cleanedString = invalidResult;
            }

            return cleanedString;

        }

        public static string FormatUsPhoneNumber(string unformattedPhoneNumber, string invalidResult = null)
        {
            string cleanedString = CleanUsPhoneNumber(unformattedPhoneNumber, invalidResult);

            return cleanedString.Length == 10
                ? string.Format("({0}) {1}-{2}",
                    cleanedString.Substring(0, 3),
                    cleanedString.Substring(3, 3),
                    cleanedString.Substring(6))
                : invalidResult == null
                    ? unformattedPhoneNumber 
                    : invalidResult;
        }

        public static bool TryGetValue<T>(this string stringValue, out T value)
        {
            object objectValue = null;
            try
            {
                objectValue = Convert.ChangeType(stringValue, typeof(T));
                value = (T)objectValue;
                return stringValue == null || objectValue != null;
            }
            catch { }

            TypeConverter converter = TypeDescriptor.GetConverter(typeof(T));
            if (converter != null)
            {
                try
                {
                    value = (T)converter.ConvertFromString(stringValue);
                    return true;
                }
                catch { }
            }
            value = default(T);
            return false;
        }

    }
}

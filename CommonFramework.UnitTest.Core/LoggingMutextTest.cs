﻿using System;
using System.Threading;
using System.Threading.Tasks;
using CommonFramework.Threads;
using Microsoft.VisualStudio.TestTools.UnitTesting;

namespace CommonFramework.UnitTest.Core
{
    [TestClass]
    [DeploymentItem("NLog.config")]
    public class LoggingMutextTest
    {
        async private Task MutexTask(LoggingMutex mutex, string description, int delay)
        {
            Guid key = await mutex.WaitAsync(description).ConfigureAwait(false);
            await Task.Delay(delay).ConfigureAwait(false);
            mutex.Release(key);
        }

        [TestMethod]
        public void TestMethod1()
        {
            LoggingMutex mutex = new LoggingMutex("Test", 100);

            Task[] tasks = 
            {
                MutexTask(mutex, "Task 1", 2000),
                MutexTask(mutex, "Task 2", 200),
                MutexTask(mutex, "Task 3", 200),
                MutexTask(mutex, "Task 4", 200)
            };
            Thread.Sleep(4000);
            bool waited = Task.WaitAll(tasks, 2000);
            Assert.IsTrue(waited, "waited");

        }
    }
}

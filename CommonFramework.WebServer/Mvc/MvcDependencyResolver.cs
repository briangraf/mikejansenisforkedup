﻿namespace CommonFramework.WebServer.Mvc
{
    using System;
    using System.Collections.Generic;
    using System.Web.Mvc;
    using CommonFramework.Container;

    public class MvcDependencyResolver: IDependencyResolver
    {
        private readonly IContainerManager _containerManager;

        public MvcDependencyResolver(IContainerManager cm)
        {
            _containerManager = cm;
        }

        public object GetService(Type serviceType)
        {
            return _containerManager.TryGetInstance(serviceType);
        }

        public IEnumerable<object> GetServices(Type serviceType)
        {
            return _containerManager.GetAll(serviceType);
        }
    }
}

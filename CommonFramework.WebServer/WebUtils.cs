﻿namespace CommonFramework.WebServer
{
    using System;
    using System.Reflection;
    using System.Web;
    using NLog;
    using CommonFramework.Log;
    using CommonFramework.Stream;

    public static class WebUtils
    {
        private static readonly Logger logger = LogManager.GetCurrentClassLogger();

        public static void RedirectToAppPath(HttpContext context, string path, params object[] args)
        {
            RedirectToAppPath(context.Request, context.Response, path, args);
        }

        public static void RedirectToAppPath(HttpRequest request, HttpResponse response, string path, params object[] args)
        {
            MethodBase method = MethodBase.GetCurrentMethod();
            logger.Enter(method, "request", request, "response", response, "path", path, "args", args);

            try
            {
                string formattedPath = args != null && args.Length > 0
                ? string.Format(path, args)
                : path;

                string finalPath = string.Format("{0}/{1}",
                    request.ApplicationPath.TrimEnd('/'),
                    formattedPath.TrimStart('/'));

                logger.Debug("finalPath: {0}", finalPath);

                response.Redirect(finalPath); 
            }
            catch (Exception ex)
            {
                logger.ErrorException("Unhandled exception", ex);
                throw;
            }
            finally
            {
                logger.Exit(method);
            }
        }

        public static void LogRequestAndResponse(HttpContext context, Logger requestLogger)
        {
            if (requestLogger.IsDebugEnabled)
            {
                requestLogger.Debug("Request: {0}", context.Request.RawUrl);
                requestLogger.Debug(" Method: {0}", context.Request.HttpMethod);
                foreach (string key in context.Request.Headers.AllKeys)
                {
                    requestLogger.Debug(" Header: {0} = {1}", key, context.Request.Headers[key]);
                }

                context.Request.Filter = new LoggingStream(context.Request.Filter, requestLogger, LogLevel.Debug, "HTTP Request",
                    LoggingStreamOptions.AsText | LoggingStreamOptions.LogReads);
                //requestLogger.WriteStreamAsText(LogLevel.Debug, context.Request.InputStream, "HTTP Request");
                LoggingStream loggingStream =  new LoggingStream(context.Response.Filter,
                    requestLogger, LogLevel.Debug, "HTTP Response", LoggingStreamOptions.AsText | LoggingStreamOptions.LogWrites);
                loggingStream.Closed += (stream) =>
                {
                    HttpResponse response = context.Response;
                    stream.Logger.Log(stream.LogLevel, "HTTP Response: {0}", response.StatusCode);
                    stream.Logger.Log(stream.LogLevel, " Content-type: {0}", response.ContentType);
                    foreach (string key in response.Headers.AllKeys)
                    {
                        stream.Logger.Log(stream.LogLevel, " Header: {0} = {1}", key, response.Headers[key]);
                    }
                };
                context.Response.Filter = loggingStream;
            }
        }
    }
}

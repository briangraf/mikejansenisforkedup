﻿namespace CommonFramework.WebServer.SignalR
{
    using System;
    using System.Collections.Generic;
    using CommonFramework.Container;
    using Microsoft.AspNet.SignalR;

    public class SignalRDependencyResolver : DefaultDependencyResolver
    {
        private readonly IContainerManager _containerManager;

        public SignalRDependencyResolver(IContainerManager containerManager)
        {
            _containerManager = containerManager;
        }

        public override object GetService(Type serviceType)
        {
            object service = _containerManager.TryGetInstance(serviceType);
            return service ?? base.GetService(serviceType);
        }

        public override IEnumerable<object> GetServices(Type serviceType)
        {
            IEnumerable<object> services = _containerManager.GetAll(serviceType);
            return services ?? base.GetServices(serviceType);
        }

    }
}

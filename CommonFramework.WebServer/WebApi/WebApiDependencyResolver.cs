﻿namespace CommonFramework.WebServer.WebApi
{
    using System;
    using System.Collections.Generic;
    using System.Web.Http.Dependencies;
    using CommonFramework.Container;

    public class WebApiDependencyResolver : IDependencyResolver
    {
        private readonly IContainerManager _containerManager;

        public WebApiDependencyResolver(IContainerManager containerManager)
        {
            _containerManager = containerManager;
        }

        public IDependencyScope BeginScope()
        {
            return this;
        }

        public object GetService(Type serviceType)
        {
            object service = _containerManager.TryGetInstance(serviceType);
            return service;
        }

        public IEnumerable<object> GetServices(Type serviceType)
        {
            IEnumerable<object> services = _containerManager.GetAll(serviceType);
            return services;
        }

        public void Dispose()
        {
        }
}
}

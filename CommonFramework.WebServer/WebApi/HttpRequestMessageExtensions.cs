﻿namespace CommonFramework.WebServer.WebApi
{
    using System;
    using System.Collections.Generic;
    using System.Net;
    using System.Net.Http;
    using System.Net.Http.Formatting;
    using System.Web.Http;
    using NLog;

    public static class HttpRequestMessageExtensions
    {
        private static readonly Logger logger = LogManager.GetCurrentClassLogger();

        public static ObjectContent<T> CreateObjectContent<T>(this HttpRequestMessage request, T value)
        {
            HttpConfiguration configuration = request.GetConfiguration();
            IContentNegotiator contentNegotiator = configuration.Services.GetContentNegotiator();
            IEnumerable<MediaTypeFormatter> formatters = configuration.Formatters;
            ContentNegotiationResult result = contentNegotiator.Negotiate(typeof(T), request, formatters);

            return result == null
                ? null
                : new ObjectContent<T>(value, result.Formatter, result.MediaType);
        }

        public static HttpResponseMessage GetResponseWithContent<T>(this HttpRequestMessage request, HttpStatusCode statusCode,
            T content)
        {
            HttpResponseMessage response = new HttpResponseMessage(statusCode);
            response.Content = request.CreateObjectContent(content);
            return response;
        }

        public static HttpResponseMessage GetRestExceptionResponse(this HttpRequestMessage request, HttpStatusCode statusCode,
            string code, string message, string userMessage = null, string reference = null)
        {
            return request.GetResponseWithContent(statusCode, new CommonExceptionBody
            {
                Code = code, 
                Message = message, 
                UserMessage = userMessage ?? message,
                Reference = reference
            });
         }

        public static HttpResponseMessage GetRestExceptionResponse(this HttpRequestMessage request,
            CommonException exception, HttpStatusCode statusCode)
        {
            return request.GetResponseWithContent(statusCode, exception.GetBody());
        }

        public static HttpResponseMessage GetRestExceptionResponse(this HttpRequestMessage request,
            Exception exception, HttpStatusCode statusCode)
        {
            return
                exception is CommonException
                    ? GetRestExceptionResponse(request, (CommonException)exception, statusCode)
                    : GetRestExceptionResponse(request, statusCode, exception.GetType().FullName, exception.Message);
        }

        public static HttpResponseMessage GetRestExceptionResponse(this HttpRequestMessage request, Exception exception)
        {
            HttpStatusCode statusCode = HttpStatusCode.InternalServerError;

            if (exception is CommonException)
            {
                CommonException cex = exception as CommonException;
                if (cex.RecommendedHttpStatusCode.HasAValue())
                {
                    statusCode = cex.RecommendedHttpStatusCode.Value;
                }
                return GetRestExceptionResponse(request, cex, statusCode);
            }
            return GetRestExceptionResponse(request, statusCode, exception.GetType().FullName, exception.Message);
        }
    }
}

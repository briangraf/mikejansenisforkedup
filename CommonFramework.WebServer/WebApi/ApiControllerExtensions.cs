﻿namespace CommonFramework.WebServer.WebApi
{
    using System;
    using System.Web.Http;
    using NLog;

    public static class ApiControllerExtensions
    {
        private static readonly Logger logger = LogManager.GetCurrentClassLogger();

        public static Uri GetResourceUri<T>(this ApiController controller, T newResource, string routeName)
        {
            string locationUrl = controller.Url.Route(routeName, new { id = newResource });
            return new Uri(controller.Request.RequestUri, locationUrl);
        }

        public static Uri GetResourceUri<T>(this ApiController controller, T newResource)
        {
            return GetResourceUri<T>(controller, newResource, "DefaultApi");
        }

    }
}

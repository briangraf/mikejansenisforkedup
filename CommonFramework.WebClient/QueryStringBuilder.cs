﻿namespace CommonFramework.WebClient
{
    using System.Collections.Generic;
    using System.Collections.Specialized;
    using System.Web;

    public class QueryStringBuilder : Dictionary<string, object>
    {
        public override string ToString()
        {
            return ToString(this);
        }

        public static string ToString(IEnumerable<KeyValuePair<string, object>> variables)
        {
            NameValueCollection collection = HttpUtility.ParseQueryString(string.Empty);
            foreach (KeyValuePair<string, object> variable in variables)
            {
                collection[variable.Key] = variable.Value.ToString();
            }
            return collection.Count == 0 ? string.Empty : "?" + collection.ToString();
        }
    }
}

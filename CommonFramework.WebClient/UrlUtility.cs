﻿namespace CommonFramework.WebClient
{
    using System.Web;

    public static class UrlUtility
    {
        public static string Encode(object value)
        {
            return HttpUtility.UrlEncode(value.ToString());
        }
    }
}

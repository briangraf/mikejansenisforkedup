﻿namespace CommonFramework.WebClient.Rest
{
    using System;
    using System.IO;
    using System.Linq;
    using System.Net;
    using System.Net.Http;
    using System.Net.Http.Formatting;
    using System.Net.Http.Headers;
    using System.Runtime.Serialization;
    using System.Threading.Tasks;
    using Newtonsoft.Json;
    using NLog;

    [DataContract(Namespace=CoreUtils.Namespace)]
    public abstract class RestClientBase
    {
        #region Private Classes

        private class FormatterLogger : IFormatterLogger
        {
            public void LogError(string errorPath, Exception exception)
            {
                logger.ErrorException(errorPath, exception);
            }

            public void LogError(string errorPath, string errorMessage)
            {
                logger.Error(errorPath + ": " + errorMessage);
            }
        }

        #endregion Private Classes

        #region Private Fields

        private static Logger logger = LogManager.GetCurrentClassLogger();

        private readonly HttpClient _httpClient;
        private readonly Uri _baseUri;
        private IRestCredential _credential;
        private string _basicAuthenticationValue;
        private AuthenticationHeaderValue _basicAuthenticationHeaderValue;

        private readonly FormatterLogger _formatterLogger = new FormatterLogger();

        #endregion Private Fields

        #region Protected Properties

        protected IRestCredential Credential
        {
            get { return _credential; }
            set { SetCredential(value); }
        }

        #endregion Protected Properties

        #region Protected Methods

        protected RestClientBase(Uri baseUri, IRestCredential credential)
        {
            _baseUri = baseUri;
            _httpClient = new HttpClient
                {
                    BaseAddress = baseUri
                };

            SetCredential(credential);
        }

        protected HttpRequestMessage NewRequest(HttpMethod method, string relativeUri)
        {
            HttpRequestMessage request = new HttpRequestMessage(method, relativeUri);
            request.Headers.Authorization = _basicAuthenticationHeaderValue;
            return request;
        }

        protected void AddRequestContent<T>(HttpRequestMessage request, T content)
        {
            JsonMediaTypeFormatter formatter = GetJsonFormatter();
            request.Content = new ObjectContent<T>(content, formatter);
        }

        async protected Task<HttpResponseMessage> SendAsync(HttpRequestMessage request)
        {
            return await _httpClient.SendAsync(request);
        }

        async protected Task<HttpResponseMessage> SendAsync(HttpMethod method, string relativeUri)
        {
            HttpRequestMessage request = NewRequest(method, relativeUri);
            return await SendAsync(request);
        }

        async protected Task<HttpResponseMessage> SendAsync<T>(HttpMethod method, string relativeUri, T content)
        {
            HttpRequestMessage request = NewRequest(method, relativeUri);
            AddRequestContent(request, content);
            return await SendAsync(request);
        }

        async protected Task<HttpResponseMessage> SendAsync(HttpMethod method, string relativeUri, Stream stream, string contentType)
        {
            HttpRequestMessage request = NewRequest(method, relativeUri);
            request.Content = new StreamContent(stream);
            request.Content.Headers.ContentType = new MediaTypeHeaderValue(contentType);
            return await SendAsync(request);
        }

        async protected Task<T> SendAndReceiveAsync<T>(HttpMethod method, string relativeUri, params HttpStatusCode[] successCodes)
        {
            HttpResponseMessage response = await SendAsync(method, relativeUri);
            return await ProcessAndUnwrapResponse<T>(response, successCodes);
        }

        async protected Task<TResult> SendAndReceiveAsync<TRequest, TResult>(HttpMethod method, string relativeUri, TRequest content,
            params HttpStatusCode[] successCodes)
        {
            HttpResponseMessage response = await SendAsync(method, relativeUri, content);
            return await ProcessAndUnwrapResponse<TResult>(response, successCodes);
        }

        async protected Task<HttpResponseMessage> PostAsync(string relativeUri)
        {
            return await SendAsync(HttpMethod.Post, relativeUri);
        }

        async protected Task<HttpResponseMessage> PostAsync<T>(string relativeUri, T content)
        {
            return await SendAsync(HttpMethod.Post, relativeUri, content);
        }

        async protected Task<HttpResponseMessage> PostAsync(string relativeUri, Stream stream, string contentType)
        {
            return await SendAsync(HttpMethod.Post, relativeUri, stream, contentType);
        }

        async protected Task<TResult> PostAsync<TRequest, TResult>(string relativeUri, TRequest content,
            params HttpStatusCode[] successCodes)
        {
            return await SendAndReceiveAsync<TRequest, TResult>(HttpMethod.Post, relativeUri, content, successCodes);
        }

        async protected Task<T> PostAsync<T>(string relativeUri,
            Stream stream, string contentType, params HttpStatusCode[] successCodes)
        {
            HttpResponseMessage response = await PostAsync(relativeUri, stream, contentType);
            return await ProcessAndUnwrapResponse<T>(response, successCodes);
        }

        async protected Task<HttpResponseMessage> GetAsync(string relativeUri)
        {
            return await SendAsync(HttpMethod.Get, relativeUri);
        }

        async protected Task<T> GetAsync<T>(string relativeUri, params HttpStatusCode[] successCodes)
        {
            return await SendAndReceiveAsync<T>(HttpMethod.Get, relativeUri, successCodes);
        }

        async protected Task<Stream> GetStreamAsync(string relativeUri)
        {
            HttpRequestMessage request = NewRequest(HttpMethod.Get, relativeUri);
            request.Headers.ConnectionClose = true;
            HttpResponseMessage response = await SendAsync(request);
            return await response.Content.ReadAsStreamAsync();
        }

        async protected Task DeleteAsync(string relativeUri)
        {
            HttpResponseMessage response = await SendAsync(HttpMethod.Delete, relativeUri);
            response.EnsureSuccessStatusCode();
        }

        protected T UnwrapResponse<T>(RestResponse<T> response)
        {
            if (response.IsData)
            {
                return response.Data;
            }
            throw new RestException(response.Message, response.Exception);
        }

        #endregion Protected Methods

        #region Private Methods

        private static JsonMediaTypeFormatter GetJsonFormatter()
        {
            JsonMediaTypeFormatter formatter = new JsonMediaTypeFormatter();
            formatter.SerializerSettings = GetJsonSerializerSettings();
            return formatter;
        }

        private static JsonSerializerSettings GetJsonSerializerSettings()
        {
            JsonSerializerSettings settings = new JsonSerializerSettings
            {
                DateTimeZoneHandling = DateTimeZoneHandling.Utc,
                Formatting = Formatting.Indented,
                //TypeNameHandling = TypeNameHandling.Objects,
                ReferenceLoopHandling = ReferenceLoopHandling.Serialize,
                PreserveReferencesHandling = PreserveReferencesHandling.Objects
            };
            return settings;
        }

        private void SetCredential(IRestCredential credential)
        {
            _credential = credential;
            _basicAuthenticationValue = credential.GetBasicAuthenticationString();
            _basicAuthenticationHeaderValue = new AuthenticationHeaderValue("Basic", _basicAuthenticationValue);
        }

        async private Task<RestResponse<T>> ProcessResponse<T>(HttpResponseMessage response, params HttpStatusCode[] successCodes)
        {
            if ((successCodes.Length > 0 && !successCodes.Contains(response.StatusCode)) ||
                (successCodes.Length == 0 && !response.IsSuccessStatusCode))
            {
                if (response.Content != null)
                {
                    try
                    {
                        CommonExceptionBody body = await ReadResponse<CommonExceptionBody>(response);
                        return new RestResponse<T>(response, body);
                    }
                    catch { }
                }
                return new RestResponse<T>(response, null);
            }
            T content = await ReadResponse<T>(response);
            return new RestResponse<T>(response, content);
        }

        private async Task<T> ReadResponse<T>(HttpResponseMessage response)
        {
            Stream stream = await response.Content.ReadAsStreamAsync();
            JsonSerializer serializer = JsonSerializer.Create(GetJsonSerializerSettings());
            JsonReader reader = new JsonTextReader(new StreamReader(stream));
            T content = serializer.Deserialize<T>(reader);
            //JsonMediaTypeFormatter formatter = GetJsonFormatter();
            //T content = (T)await formatter.ReadFromStreamAsync(typeof(T), stream, response.Content, _formatterLogger);
            return content;
        }

        async private Task<T> ProcessAndUnwrapResponse<T>(HttpResponseMessage response, params HttpStatusCode[] successCodes)
        {
            RestResponse<T> restResponse = await ProcessResponse<T>(response, successCodes);
            return UnwrapResponse(restResponse);
        }

        #endregion Private Methods
    }
}
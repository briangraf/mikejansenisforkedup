﻿namespace CommonFramework.WebClient.Rest
{
    using System.Net.Http;

    public class RestException: CommonException
    {
        public HttpResponseMessage Response { get; private set; }

        public RestException(string message) : base(message) { }

        //public RestException(HttpResponseMessage response)
        //{
        //    // TODO: do something with this
        //    Response = response;
        //}

        public RestException(HttpResponseMessage response, CommonExceptionBody body) :
            base(body)
        {
            Response = response;
        }
    }
}

﻿namespace CommonFramework.WebClient.Rest
{
    public class AnonymousRestCredential: RestCredentialBase
    {
        private AnonymousRestCredential() { }

        private static readonly AnonymousRestCredential _instance = new AnonymousRestCredential();
        public static IRestCredential Instance { get { return _instance; } }
    }
}

﻿namespace CommonFramework.WebClient.Rest
{
    using System;

    public interface IRestCredential
    {
        string GetBasicAuthenticationString();
        void InitializeFromBasicAuthentication(string basicAuthentication);
    }
}

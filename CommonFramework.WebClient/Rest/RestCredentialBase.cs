﻿namespace CommonFramework.WebClient.Rest
{
    using System;
    using System.Collections.Generic;
    using System.Collections.ObjectModel;
    using System.Linq;
    using System.Runtime.Serialization;
    using System.Text;

    public enum RestCredentialFieldPlacement
    {
        BasicUser,
        BasicPassword
    }


    [DataContract(Namespace = CoreUtils.Namespace)]
    public abstract class RestCredentialBase : IRestCredential
    {
        #region Private Fields and Declarations

        private readonly char BasicAuthDelimiter = ':';
        private readonly char TokenDelimeter = '|';
        private readonly char Equals = '=';

        protected class CredentialField
        {
            public string Key { get; set; }
            public string Name { get; set; }
            public object Value { get; set; }
            public string StringValue { get; set; }
            public bool IsDirty { get; set; }
            public RestCredentialFieldPlacement Placement { get; set; }
        }

        private readonly Dictionary<string, CredentialField> _fields = new Dictionary<string, CredentialField>();
        private readonly ReadOnlyDictionary<string, CredentialField> _readOnlyFields;

        #endregion

        #region Protected Members

        protected RestCredentialBase()
        {
            _readOnlyFields = new ReadOnlyDictionary<string, CredentialField>(_fields);
        }

        protected IReadOnlyDictionary<string,CredentialField> Fields { get { return _readOnlyFields; } }

        protected void RegisterField(string key, string name, RestCredentialFieldPlacement placement)
        {
            _fields[key] = new CredentialField { Key = key, Name = name, Placement = placement, IsDirty = true };
        }

        #endregion

        #region Public Properties and Methods

        public void InitializeFromBasicAuthentication(string basicAuthentication)
        {
            string unwrapped = Encoding.ASCII.GetString(Convert.FromBase64String(basicAuthentication));
            if (unwrapped.Length == 0 || unwrapped[0] == BasicAuthDelimiter) return;
            IEnumerable<KeyValuePair<string, string>> parts = unwrapped
                                            .Split(BasicAuthDelimiter)
                                            .SelectMany(s => s.Split(TokenDelimeter))
                                            .Select(s =>
                                            {
                                                string[] keyvalue = s.Split(Equals);
                                                return new KeyValuePair<string, string>(keyvalue[0], keyvalue[1]);
                                            });
            foreach (KeyValuePair<string, string> part in parts)
            {
                string key = part.Key;
                string value = part.Value;
                SetStringValue(key, value);
            }
        }

        public string GetBasicAuthenticationString()
        {
            return Convert.ToBase64String(
                Encoding.ASCII.GetBytes(GetUserString() + BasicAuthDelimiter + GetPasswordString()));
        }

        #endregion

        #region Protected Methods

        protected void Set<T>(string key, T value)
        {
            CredentialField field = _fields[key];
            field.Value = value;
            field.StringValue = object.Equals(value, default(T)) ? null : value.ToString();
            field.IsDirty = false;
        }

        protected T Get<T>(string key)
        {
            CredentialField field = _fields[key];
            if (field.IsDirty)
            {
                T value;
                field.Value = field.StringValue != null && field.StringValue.TryGetValue(out value)
                    ? value
                    : default(T);
                field.IsDirty = false;
            }
            return (T)field.Value;
        }

        #endregion

        #region Private Methods

        private void AddNonNull(ICollection<string> collection, string key, string value)
        {
            if (!string.IsNullOrWhiteSpace(value))
            {
                collection.Add(string.Format("{0}{2}{1}", key, value, Equals));
            }
        }

        private void SetStringValue(string key, string value)
        {
            CredentialField field = _fields[key];
            field.StringValue = value;
            field.IsDirty = true;
        }

        private string GetTokenString(RestCredentialFieldPlacement placement)
        {
            List<string> tokens = new List<string>();
            foreach (CredentialField field in _fields.Values.Where(f => f.Placement == placement && f.Value != null))
            {
                AddNonNull(tokens, field.Key, field.Value.ToString());
            }
            return string.Join(TokenDelimeter.ToString(), tokens);
        }

        private string GetUserString()
        {
            return GetTokenString(RestCredentialFieldPlacement.BasicUser);
        }

        private string GetPasswordString()
        {
            return GetTokenString(RestCredentialFieldPlacement.BasicPassword);
        }

        #endregion
    }
}

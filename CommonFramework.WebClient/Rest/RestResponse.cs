﻿namespace CommonFramework.WebClient.Rest
{
    using System.Net.Http;

    public class RestResponse<T>
    {
        public bool IsData { get; private set; }
        public HttpResponseMessage Message { get; private set; }
        public T Data { get; private set; }
        public CommonExceptionBody Exception { get; private set; }

        public RestResponse(HttpResponseMessage message, T data)
        {
            Message = message;
            Data = data;
            IsData = true;
        }

        public RestResponse(HttpResponseMessage message, CommonExceptionBody exceptionBody)
        {
            Message = message;
            Data = default(T);
            IsData = false;
            Exception = exceptionBody;
        }
    }
}

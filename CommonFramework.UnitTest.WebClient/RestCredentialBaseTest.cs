﻿using System;
using CommonFramework.WebClient.Rest;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using NLog;

namespace CommonFramework.UnitTest.WebClient
{
    public enum RestCredentialMockKind
    {
        Akind,
        Bkind,
        Ckind
    }

    public class RestCredentialMock: RestCredentialBase
    {
        public RestCredentialMock()
        {
            RegisterField("A", "A", RestCredentialFieldPlacement.BasicUser);
            RegisterField("B", "B", RestCredentialFieldPlacement.BasicUser);
            RegisterField("C", "C", RestCredentialFieldPlacement.BasicUser);
            RegisterField("D", "D", RestCredentialFieldPlacement.BasicUser);
            RegisterField("E", "E", RestCredentialFieldPlacement.BasicUser);
        }

        public int AnInt
        {
            get { return Get<int>("A"); }
            set { Set<int>("A", value); }
        }

        public int? ANullableInt
        {
            get { return Get<int?>("B"); }
            set { Set<int?>("B", value); }
        }

        public string AString
        {
            get { return Get<string>("C"); }
            set { Set<string>("C", value); }
        }

        public RestCredentialMockKind Kind
        {
            get { return Get<RestCredentialMockKind>("D"); }
            set { Set<RestCredentialMockKind>("D", value); }
        }

        public RestCredentialMockKind? NullableKind
        {
            get { return Get<RestCredentialMockKind?>("E"); }
            set { Set<RestCredentialMockKind?>("E", value); }
        }

    }

    [TestClass]
    public class RestCredentialBaseTest
    {
        private static readonly Logger logger = LogManager.GetCurrentClassLogger();

        [TestMethod]
        public void TestMethod1()
        {
            RestCredentialMock target = new RestCredentialMock();

            Assert.AreEqual(0, target.AnInt, "target.AnInt");
            Assert.IsNull(target.ANullableInt, "target.ANullableInt");
            Assert.IsNull(target.AString, "target.AString");
            Assert.AreEqual(RestCredentialMockKind.Akind, target.Kind, "target.Kind");
            Assert.IsNull(target.NullableKind, "target.NullableKind");

            target.AnInt = 10;
            target.ANullableInt = 20;
            target.AString = "this";
            target.Kind = RestCredentialMockKind.Bkind;
            target.NullableKind = RestCredentialMockKind.Ckind;

            Assert.AreEqual(10, target.AnInt, "target.AnInt");
            Assert.AreEqual(20, target.ANullableInt, "target.ANullableInt");
            Assert.AreEqual("this", target.AString, "target.AString");
            Assert.AreEqual(RestCredentialMockKind.Bkind, target.Kind, "target.Kind");
            Assert.AreEqual(RestCredentialMockKind.Ckind, target.NullableKind, "target.NullableKind");

        }
    }
}

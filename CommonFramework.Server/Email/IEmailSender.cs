﻿namespace CommonFramework.Server.Email
{
    public interface IEmailSender
    {
        bool Send(string to, string subject, string body, bool isHtml);
    }
}

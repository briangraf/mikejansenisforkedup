﻿namespace CommonFramework.Server.Email
{
    using System;
    using System.Net;
    using System.Net.Mail;
    using System.Reflection;
    using NLog;
    using CommonFramework.Log;

    public class DefaultEmailSender: IEmailSender
    {
        private static Logger logger = LogManager.GetCurrentClassLogger();

        private readonly string m_FromAddress;
        private readonly string m_SmtpUserName;
        private readonly string m_SmtpPassword;
        private readonly string m_SmtpServer;
        private readonly int m_SmtpPort;
        private readonly bool m_UseSsl;

        private readonly ICredentialsByHost m_SmtpCredentials;

        private readonly SmtpClient m_SmtpClient;

        public DefaultEmailSender()
        {
            throw new NotImplementedException();

            // TODO: Initialization
            m_FromAddress = null;
            m_SmtpUserName = null;
            m_SmtpPassword = null;
            m_SmtpServer = null;
            m_SmtpPort = 0;
            m_UseSsl = false;

            m_SmtpClient = new SmtpClient();
            m_SmtpClient.Host = m_SmtpServer;
            if (m_SmtpPort != 0) m_SmtpClient.Port = m_SmtpPort;
            m_SmtpClient.EnableSsl = m_UseSsl;

            m_SmtpCredentials = string.IsNullOrWhiteSpace(m_SmtpUserName) 
                ? null
                : new NetworkCredential(m_SmtpUserName, m_SmtpPassword);

        }

        public bool Send(string to, string subject, string body, bool isHtml)
        {
            MethodBase method = MethodBase.GetCurrentMethod();
            logger.Enter(method);

            try
            {
                MailMessage message = new MailMessage(m_FromAddress, to, subject, body);
                message.IsBodyHtml = isHtml;
                m_SmtpClient.Credentials = m_SmtpCredentials;
                m_SmtpClient.Send(message);
                return true;

            }
            catch (Exception ex)
            {
                logger.FullException(LogLevel.Error, ex, "Unhandled exception");
                return false;
            }
            finally
            {
                logger.Exit(method);
            }
        }



    }
}
